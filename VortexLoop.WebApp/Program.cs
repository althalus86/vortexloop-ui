using Fluxor;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Syncfusion.Blazor;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // Add your Syncfusion license key for Blazor platform with corresponding Syncfusion NuGet version referred in project. For more information about license key see https://help.syncfusion.com/common/essential-studio/licensing/license-key.
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("MzczNjI2QDMxMzgyZTM0MmUzMGd3LzZuYmgzd0lmWmlMM09SbHpzWkp5alArRVVoanM4Lzd6OHNYQ096M2s9;MzczNjI3QDMxMzgyZTM0MmUzMFI0VTAxMWRiMVNQd2NWVTV2OGhtNEQ4VzdWNFZ4Vm9WYXMxWkovZkp2SXc9;MzczNjI4QDMxMzgyZTM0MmUzME1OYzRPL1RUZXpERFBXUngzd3FGblFsT2ZCSlRFNjV1WGR2RWtJeUl3azA9;MzczNjI5QDMxMzgyZTM0MmUzMERpQWJhdTNyTGxEaStySTgxQnB1N1o5Z3JYUm9LejkycUovQ01USUtGSTA9;MzczNjMwQDMxMzgyZTM0MmUzMFpMNXlTb09rbWdxWVhkYVhiNHpUT0p3TDMxbVJXcHFodFM3T0pFdUtXYWc9;MzczNjMxQDMxMzgyZTM0MmUzMFBKbEZXMUJmRWIrM2U3eWI0ZmVWdVZ6TWNkMkFzTmJuOTJwSDdjeW4ya289;MzczNjMyQDMxMzgyZTM0MmUzMEVqSSsvVis4RDB1S3VIRVJWK1k1OGhLbjB6K2loZTFySDZsNUxwUjc1eUE9;MzczNjMzQDMxMzgyZTM0MmUzMFZ5cUFyZFZGalkrVHRIUEJzTks2dWFTKy84U1hWR1NqcEZLaXkwdkNxbUU9;MzczNjM0QDMxMzgyZTM0MmUzMFhFUUVjYUp1VE02OXpjNjZscGVZTWJVWHJEekZQNGQ4RXRBb2ZhSVg0Q0E9;MzczNjM1QDMxMzgyZTM0MmUzMGMxWlF3Y3hVNjVRU1pQaTEyZ29VR09oR3FldmdyL1hiTUtmVVc5dEZJMWs9");

            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            
            if(builder.HostEnvironment.IsProduction())
            {
                // Production
                Console.WriteLine("Setting up Production Environment");
                
            }
            else if (builder.HostEnvironment.IsDevelopment())
            {
                // Development
                Console.WriteLine("Setting up Development Environment");
                //await Task.Delay(10000);                
            }
            else
            {
                Console.WriteLine("ERROR: No ASPNETCORE_ENVIRONMENT Configured in Environment Variables");
                throw new Exception("NO ASPNETCORE_ENVIRONMENT SET");
            }


            if (builder.Configuration != null)
            {
                string env = builder.Configuration["ENVIRONMENT_MODE"];
                Console.WriteLine("Tried to get Config: ENVIRONMENT_MODE = {" + env + "} - from " + "MAIN PROGRAM TASK");
            }
            else
            {
                Console.WriteLine("Tried to get Config: Config is NULL - from " + "MAIN PROGRAM TASK");
            }

            var appSettings = GetAppSettings(builder.Configuration);
            Console.WriteLine(appSettings.ENVIRONMENT_MODE);


            builder.RootComponents.Add<App>("#app");

            builder.Services.AddScoped(sp => new HttpClient
            {
                BaseAddress = new Uri(builder.HostEnvironment.BaseAddress)
            });

            // Adding Fluxor (State Store)
            builder.Services.AddFluxor(options => options.ScanAssemblies(typeof(Program).Assembly));

            builder.Services.AddOidcAuthentication(options =>
            {
                // Configure your authentication provider options here.
                // For more information, see https://aka.ms/blazor-standalone-auth
                builder.Configuration.Bind("Local", options.ProviderOptions);
                options.ProviderOptions.ResponseType = "code";
                options.ProviderOptions.DefaultScopes.Add("profile");
            });

            builder.Services.AddSyncfusionBlazor();

            await builder.Build().RunAsync();
            
        }

        //public static void ConfigureServices(IServiceCollection services)
        //{
        //    Console.WriteLine("In Configure Serices");
        //    // Example of loading a configuration as configuration isn't available yet at this stage.
        //    services.AddSingleton(provider =>
        //    {
        //        var config = provider.GetService<IConfiguration>();
        //        Console.WriteLine("Getting Config Instance " + config.ToString());
        //        return config.GetSection("App").Get<AppSetting>();
        //    });
        //}

        private static AppSettingModel GetAppSettings(WebAssemblyHostConfiguration Configuration)
        {
            AppSettingModel AppSettings = new AppSettingModel();
            if (Configuration != null)
            {
                AppSettings.ENVIRONMENT_MODE = Configuration[AppSettingModel.ENVIRONMENT_MODE_KEY];
                AppSettings.ENVIRONMENT_APP_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_APP_BASE_URL_KEY];
                AppSettings.ENVIRONMENT_API_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_API_BASE_URL_KEY];
                AppSettings.ENVIRONMENT_API_PROXY_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_API_PROXY_BASE_URL_KEY];
                AppSettings.RELAY_RESOURCE_ROUTE = Configuration[AppSettingModel.RELAY_RESOURCE_ROUTE_KEY];
                AppSettings.RELAY_USE_PROXY = Configuration[AppSettingModel.RELAY_USE_PROXY_KEY];
                AppSettings.RELAY_USE_MOCK = Configuration[AppSettingModel.RELAY_USE_MOCK_KEY];
                AppSettings.TIMESLOT_RESOURCE_ROUTE = Configuration[AppSettingModel.TIMESLOT_RESOURCE_ROUTE_KEY];
                AppSettings.TIMESLOT_USE_PROXY = Configuration[AppSettingModel.TIMESLOT_USE_PROXY_KEY];
                AppSettings.TIMESLOT_USE_MOCK = Configuration[AppSettingModel.TIMESLOT_USE_MOCK_KEY];
                AppSettings.SETTING_RESOURCE_ROUTE = Configuration[AppSettingModel.SETTING_RESOURCE_ROUTE_KEY];
                AppSettings.SETTING_USE_PROXY = Configuration[AppSettingModel.SETTING_USE_PROXY_KEY];
                AppSettings.SETTING_USE_MOCK = Configuration[AppSettingModel.SETTING_USE_MOCK_KEY];
            }
            return AppSettings;
        }

    }
}
