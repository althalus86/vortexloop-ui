﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Store.RelaysUseCase
{
	public class FetchRelaysDataResultAction
	{
		public Dictionary<string, RelayDataModel> Relays { get; }

		public FetchRelaysDataResultAction(Dictionary<string, RelayDataModel> relays)
		{
			Relays = relays;
		}
	}
}
