﻿using Fluxor;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net.Http.Json;
using VortexLoop.BlazorApp.Models;
using System.Collections.Generic;

namespace VortexLoop.BlazorApp.Store.RelaysUseCase
{
	public class Effects
	{
		private readonly HttpClient Http;
		public Effects(HttpClient http)
		{
			Http = http;
		}

		[EffectMethod]
		public async Task HandleFetchRelaysDataAction(FetchRelaysDataAction action, IDispatcher dispatcher)
		{
			var relaysResultList = await GetRelayKeyValueList(); //TODO: Add Logic for OpenID
			dispatcher.Dispatch(new FetchRelaysDataResultAction(relaysResultList));
		}

		[EffectMethod]
		public async Task HandleSetRelayStateAction(SetRelayStateAction action, IDispatcher dispatcher)
		{
			var relaysResultList = await PutRelay(); //TODO: Add Logic for OpenID
			dispatcher.Dispatch(new FetchRelaysDataResultAction(relaysResultList));
		}

		private async Task<Dictionary<string, RelayDataModel>> GetRelayKeyValueList()
		{			
			return new Dictionary<string, RelayDataModel>();

			// Can do logic here to see if we should get Mock / Local / Remote Data
		}



	}
	
}
