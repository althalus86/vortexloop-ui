﻿using Fluxor;

namespace VortexLoop.BlazorApp.Store.RelaysUseCase
{
    public class Feature : Feature<RelaysStateStore>
	{
		public override string GetName() => nameof(RelaysStateStore);
		protected override RelaysStateStore GetInitialState()
        {
			return new RelaysStateStore(
				isLoading: false,
				relays: null);			
		}			
	}
}
