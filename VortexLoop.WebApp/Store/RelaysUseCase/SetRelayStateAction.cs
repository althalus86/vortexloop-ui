﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;
using static VortexLoop.BlazorApp.Models.RelayDataModel;

namespace VortexLoop.BlazorApp.Store.RelaysUseCase
{
	public class SetRelayStateAction
	{
		public string RelayId { get; }
		public RelayState NewRelayState { get; }

		public SetRelayStateAction(string relayId, RelayState newRelayState)
		{
			RelayId = relayId;
			NewRelayState = newRelayState;
		}
	}
}
