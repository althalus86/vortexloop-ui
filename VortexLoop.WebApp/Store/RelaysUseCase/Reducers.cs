﻿using Fluxor;
using System;
using System.Collections.Generic;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Store.RelaysUseCase
{
    public static class Reducers
	{
		[ReducerMethod]
		public static RelaysStateStore ReduceFetchRelaysDataAction(RelaysStateStore state, FetchRelaysDataAction action) =>
			new RelaysStateStore(
				isLoading: true,
				relays: null);

		[ReducerMethod]
		public static RelaysStateStore ReduceFetchRelaysDataResultAction(RelaysStateStore state, FetchRelaysDataResultAction action) =>
			new RelaysStateStore(
				isLoading: false,
				relays: action.Relays);

		[ReducerMethod]
		public static RelaysStateStore ReduceSetRelayStateAction(RelaysStateStore state, SetRelayStateAction action)
        {
			Dictionary<string, RelayDataModel> newRelaysStateStore = state.Relays;
			
			RelayDataModel newRelayDataModel;
			if (state.Relays.TryGetValue(action.RelayId, out newRelayDataModel))
			{
				Console.WriteLine("INFO: Relay with Id " + action.RelayId + " has State found as " + newRelayDataModel.State);
				newRelayDataModel.State = action.NewRelayState;
				newRelaysStateStore[action.RelayId] = newRelayDataModel;
				Console.WriteLine("INFO: Relay with Id " + action.RelayId + " has State updated to " + newRelayDataModel.State);
			}
            else
            {
				Console.WriteLine("INFO: Relay with Id " + action.RelayId + " no found");
            }

			return new RelaysStateStore(
				isLoading: state.IsLoading,
				relays: newRelaysStateStore
			);
		}
	}
}
