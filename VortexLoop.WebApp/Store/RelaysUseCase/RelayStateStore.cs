﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Store.RelaysUseCase
{
    public class RelaysStateStore
    {
        public bool IsLoading { get; }
        public Dictionary<string,  RelayDataModel> Relays { get; }

        public RelaysStateStore(bool isLoading, Dictionary<string, RelayDataModel> relays)
        {
            IsLoading = IsLoading;
            Relays = relays;
        }
    }
}
