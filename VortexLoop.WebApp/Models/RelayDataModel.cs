using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace VortexLoop.BlazorApp.Models
{
    // Relay Object Info
    public class RelayDataModel
    {
        public enum RelayState
        {
            Off = 0,
            On = 1
        }

        [JsonProperty("id")]
        [JsonPropertyName("id")]
        public string Id { get; set; }                  // External = "12345" | Internal = "{GUID}"        
        [JsonProperty("state")]
        [JsonPropertyName("state")]
        public RelayState State { get; set; }           // ON = 1 | OFF = 0              
        [JsonProperty("relayGroupId")]
        [JsonPropertyName("relayGroupId")]
        public string RelayGroupId { get; set; }        // Crt01 = "1" | Crt02 = "2" | Crt03 = "3" | Crt04 = "4"  
    }
    public static class RelayGroup
    {
        public static string RG_01 { get { return "1"; } }
        public static string RG_02 { get { return "2"; } }
        public static string RG_03 { get { return "3"; } }
        public static string RG_04 { get { return "4"; } }
    }
}
