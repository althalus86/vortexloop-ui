using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace VortexLoop.BlazorApp.Models
{
    // Settings Object Info
    public class SettingDataModel
    {
        [JsonProperty("latitude")]
        [JsonPropertyName("latitude")]
        public double Latitude { get; set; }                            // Ex. 35.917973
        [JsonProperty("longitude")]
        [JsonPropertyName("longitude")]
        public double Longitude { get; set; }                           // Ex. 14.409943
        [JsonProperty("sunlightAware")]
        [JsonPropertyName("sunlightAware")]
        public bool SunlightAware { get; set; }                         // false
        [JsonProperty("advanced")]
        [JsonPropertyName("advanced")]
        public AdvancedSettingDataModel AdvancedSetings { get; set; }            // { loglevel }
        [JsonProperty("relayGroups")]
        [JsonPropertyName("relayGroups")]
        public RelayGroupsSettingDataModel RelayGroupsSettings { get; set; }     // relayGroupSettings { mode }        
    }

    public class AdvancedSettingDataModel
    {
        [JsonProperty("loglevel")]
        [JsonPropertyName("loglevel")]
        public string LogLevel { get; set; }                    // "info" | "warning" | "verbose"
        [JsonProperty("keepAgendaHistoryForDays")]
        [JsonPropertyName("keepAgendaHistoryForDays")]
        public string KeepAgendaHistoryForDays { get; set; }    // "10" - Default: 10 days
        [JsonProperty("runCleanJobEveryDays")]
        [JsonPropertyName("runCleanJobEveryDays")]
        public string RunCleanJobEveryDays { get; set; }        // "1" - Default: 1 day
    }

    public static class LogLevelType
    {
        public static string lltInfo { get { return "info"; } }
        public static string lltWarning { get { return "warning"; } }
        public static string lltError { get { return "error"; } }
        public static string lltVerbose { get { return "verbose"; } }
    }

    public class RelayGroupsSettingDataModel
    {
        [JsonProperty("1")]
        [JsonPropertyName("1")]
        public GroupSettingsDataModel RG_01 { get; set; }    // { mode | sunlightOffset }
        [JsonProperty("2")]
        [JsonPropertyName("2")]
        public GroupSettingsDataModel RG_02 { get; set; }    // { mode | sunlightOffset }
        [JsonProperty("3")]
        [JsonPropertyName("3")]
        public GroupSettingsDataModel RG_03 { get; set; }    // { mode | sunlightOffset }
        [JsonProperty("4")]
        [JsonPropertyName("4")]
        public GroupSettingsDataModel RG_04 { get; set; }    // { mode | sunlightOffset }
    }

    public class GroupSettingsDataModel
    {
        [JsonProperty("mode")]
        [JsonPropertyName("mode")]
        public string Mode { get; set; }                // "manual" | "internal" | "external"
        [JsonProperty("sunlightOffset")]
        [JsonPropertyName("sunlightOffset")]
        public double SunlightOffset { get; set; }      // "0" - Default 0 Offset in Minuites

    }
}

