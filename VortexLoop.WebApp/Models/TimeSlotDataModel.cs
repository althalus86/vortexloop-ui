using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace VortexLoop.BlazorApp.Models
{
    
    public class TimeSlotDataModel
    {
        public enum TimeSlotStateDataModel
        {
            Off = 0,
            On = 1
        }

        // Time Slot Object Info
        [JsonProperty("id")]
        [JsonPropertyName("id")]
        public string Id { get; set; }                  // External = "12345" | Internal = "{GUID}"
        [JsonProperty("createdOn")]
        [JsonPropertyName("createdOn")]
        public DateTime CreatedOn { get; set; }         // "2018-02-10T09:33:59Z"
        [JsonProperty("ownerId")]
        [JsonPropertyName("ownerId")]
        public string CreatedByUserId { get; set; }     // "12341234"
        [JsonProperty("ownerFriendlyName")]
        [JsonPropertyName("ownerFriendlyName")]
        public string CreatedByUsername { get; set; }   // "Kurt Carabott"
        [JsonProperty("type")]
        [JsonPropertyName("type")]
        public string Type { get; set; }                // Booking System = "External" | Committe Schedule = "Internal"  

        // Time Slot Context Info
        [JsonProperty("state")]
        [JsonPropertyName("state")]
        public TimeSlotStateDataModel State { get; set; }        // ON = 1 | OFF = 0
        [JsonProperty("title")]
        [JsonPropertyName("title")]
        public string Title { get; set; }               // "Title Text"
        [JsonProperty("description")]
        [JsonPropertyName("description")]
        public string Description { get; set; }         // "Descriprtion text can be longer"
        [JsonProperty("timeFrom")]
        [JsonPropertyName("timeFrom")]
        public DateTime TimeFrom { get; set; }          // "2018-02-10T13:00Z"
        [JsonProperty("timeTo")]
        [JsonPropertyName("timeTo")]
        public DateTime TimeTo { get; set; }            // "2018-02-10T14:30Z"
        [JsonProperty("relayGroupId")]
        [JsonPropertyName("relayGroupId")]
        public string RelayGroupId { get; set; }        // Crt01 = "1" | Crt02 = "2" | Crt03 = "3" | Crt04 = "4"  
        [JsonProperty("sunlightAwareAffected")]
        [JsonPropertyName("sunlightAwareAffected")]
        public bool SunlightAwareAffected { get; set; } // true OR false :: Says true when lights need to be disabled/enabled mid-timeslot due to Sunrise/Sunset  

    }

    public static class TimeSlotMode
    {        
        public static string tstInternal { get { return "Internal"; } }
        public static string tstExternal { get { return "External"; } }
    }
}

