﻿using Fluxor;
using Microsoft.AspNetCore.Components;
using VortexLoop.BlazorApp.Store.RelaysUseCase;
using static VortexLoop.BlazorApp.Models.RelayDataModel;

namespace VortexLoop.BlazorApp.Pages
{
    public partial class TestPage2
    {

        [Inject]
        private IState<RelaysStateStore> RelayState { get; set; }

		[Inject]
		public IDispatcher Dispatcher { get; set; }

		private void IncrementCount()
		{
			//var action = new Store.RelaysUseCase.SetRelayStateAction(string relayId);
			//Dispatcher.Dispatch(action);
		}

	}
}
