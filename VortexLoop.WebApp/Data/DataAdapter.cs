﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Data
{
    public class DataAdapter
    {
        public string BASE_URL { get; set; }

        public AppSettingModel appSettings { get; set; }

        public DataAdapter()
        {
            Console.WriteLine("Data Adapter Constructor");
            LoadAppSettings();
        }
        
        private async void LoadAppSettings()
        {
            try
            {

            
                Console.WriteLine("Load App Settings - Start");
                AppSettingModel jsonAppSettings = new AppSettingModel();

                //var  = JsonConvert.DeserializeObject<JsonAppSettings>(myJsonString);

                Console.WriteLine(" VLUI_BASE_APP_URL " + Environment.GetEnvironmentVariable("VLUI_BASE_APP_URL"));
                Console.WriteLine(" VLUI_BASE_API_URL " + Environment.GetEnvironmentVariable("VLUI_BASE_API_URL"));
                Console.WriteLine(" VLUI_ASPNETCORE_ENVIRONMENT " + Environment.GetEnvironmentVariable("VLUI_ASPNETCORE_ENVIRONMENT"));
                // create request object
                var requestUrl = "~/appconfig.json" + "?timestamp=" + DateTime.Now.ToString("yyyyMMddHHmmss");
                Console.WriteLine(requestUrl);
                var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);

                // add authorization header
                //request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", "my-token");

                // add custom http header
                request.Headers.Add("My-Custom-Header", "foobar");
                //request.Headers.CacheControl.NoCache = true;
                //request.Headers.CacheControl.NoStore = true;

                HttpClient client = new HttpClient();

                // send request
                var httpResponse = await client.SendAsync(request);

                if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var httpResponseContentString = await httpResponse.Content.ReadAsStringAsync();
                    Console.WriteLine(httpResponseContentString);
                    // convert http response data to UsersResponse object            
                    jsonAppSettings = JsonConvert.DeserializeObject<AppSettingModel>(httpResponseContentString);
                    Console.WriteLine(JsonConvert.SerializeObject(jsonAppSettings, Formatting.Indented));
                }
                Console.WriteLine("Load App Settings - End");

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + " - " + ex.InnerException);
                throw;
            }
        }
    }


}


