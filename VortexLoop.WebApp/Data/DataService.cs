using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp
{
    public class VortexLoopDataService
    {
        [Inject] IConfiguration Configuration { get; set; }
        [Inject] IAccessTokenProvider TokenProvider { get; set; }


        // Singlton Schema
        private List<RelayDataModel> RelayLists { get; set; }
        private List<TimeSlotDataModel> TimeSlotLists { get; set; }
        private List<TimeSlotDataModel> TimeSlotExternalLists { get; set; }
        private SettingDataModel Settings { get; set; }
        private AppSettingModel AppSettings { get; set; }
        private string AccessToken { get; set; }


        // Singlton Setup
        //public static VortexLoopDataService _VLDataServiceInstance = new VortexLoopDataService(AppSettings);
        public VortexLoopDataService(AppSettingModel appSetting)
        {
            TryGetConfiguration("VortexLoopDataService() Constructor - [App]");
            // On Startup
            GetAppSettings();
            //GetInitialData();
        }

        private void GetAppSettings()
        {
            if(Configuration != null)
            {
                AppSettings.ENVIRONMENT_MODE = Configuration[AppSettingModel.ENVIRONMENT_MODE_KEY];
                AppSettings.ENVIRONMENT_APP_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_APP_BASE_URL_KEY];
                AppSettings.ENVIRONMENT_API_BASE_URL = Configuration[AppSettings.ENVIRONMENT_API_BASE_URL];
                AppSettings.ENVIRONMENT_API_PROXY_BASE_URL = Configuration[AppSettings.ENVIRONMENT_API_PROXY_BASE_URL];
                AppSettings.RELAY_RESOURCE_ROUTE = Configuration[AppSettings.RELAY_RESOURCE_ROUTE];
                AppSettings.RELAY_USE_PROXY = Configuration[AppSettings.RELAY_USE_PROXY];
                AppSettings.RELAY_USE_MOCK = Configuration[AppSettings.RELAY_USE_MOCK];
                AppSettings.TIMESLOT_RESOURCE_ROUTE = Configuration[AppSettings.TIMESLOT_RESOURCE_ROUTE];
                AppSettings.TIMESLOT_USE_PROXY = Configuration[AppSettings.TIMESLOT_USE_PROXY];
                AppSettings.TIMESLOT_USE_MOCK = Configuration[AppSettings.TIMESLOT_USE_MOCK];
                AppSettings.SETTING_RESOURCE_ROUTE = Configuration[AppSettings.SETTING_RESOURCE_ROUTE];
                AppSettings.SETTING_USE_PROXY = Configuration[AppSettings.SETTING_USE_PROXY];
                AppSettings.SETTING_USE_MOCK = Configuration[AppSettings.SETTING_USE_MOCK];
            }
        }

        private void GetInitialData()
        {
            GetAccessToken();

            GetTimeSlots();
        }

        private async Task GetAccessToken()
        {
            Console.WriteLine("Starting to RequestAccessToken");
            var accessTokenResult = await TokenProvider.RequestAccessToken();
                                   
            Console.WriteLine("Starting to TryGetToken");
            if (accessTokenResult.TryGetToken(out var token))
            {
                Console.WriteLine("Starting to TryGetToken IF IN");
                AccessToken = token.Value;
                Console.WriteLine("Token Value - " + token.Value);
            }
            else 
            {
                Console.WriteLine("Starting to TryGetToken IF ELSE");
                AccessToken = string.Empty;
            }
            Console.WriteLine("Token Variable - " + AccessToken);
        }

        // Public Methods
        //public List<TimeSlotDataModel> GetTimeSlots()
        //{
        //    if(TimeSlotLists == null)
        //    {
        //        GetAccessToken();
        //        PopulateTimeSlots();
        //    }
                

            //if (TimeSlotLists != null && TimeSlotLists.Count > 0)
            //{
            //    Console.WriteLine("TimeSlotLists != null");
            //    return _VLDataServiceInstance.TimeSlotLists;
            //}
            //else
            //{
            //    Console.WriteLine("TimeSlotLists == null");
            //    return null;
            //}
                
        }

        //public List<TimeSlotDataModel> GetTimeSlotsExternal()
        //{
        //    return _VLDataServiceInstance.TimeSlotExternalLists;
        //}

        //public List<RelayDataModel> GetRelays()
        //{
        //    return _VLDataServiceInstance.RelayLists;
        //}

        //public SettingDataModel GetSettings()
        //{
        //    return _VLDataServiceInstance.Settings;
        //}



        // Private Methods
        private void PopulateRelays()
        {
             
        }
        private async void PopulateTimeSlots()
        {
            // Check if TimeSlotLists is initated
            if (TimeSlotLists == null)
                TimeSlotLists = new List<TimeSlotDataModel>();

            var requestUrl = AppSettings.ENVIRONMENT_API_BASE_URL + AppSettings.TIMESLOT_RESOURCE_ROUTE;
            var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
            request.Headers.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", AccessToken);
            HttpClient client = new HttpClient();
            var httpResponse = await client.SendAsync(request);
            if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                Console.WriteLine("Response OK");
                var httpResponseContentString = await httpResponse.Content.ReadAsStringAsync();
                Console.WriteLine(httpResponseContentString);
                TimeSlotLists = JsonConvert.DeserializeObject<List<TimeSlotDataModel>>(httpResponseContentString);
                Console.WriteLine(JsonConvert.SerializeObject(TimeSlotLists, Formatting.Indented));
            }
            else
            {
                Console.WriteLine("Response Error - " + httpResponse.StatusCode.ToString());
            }
            Console.WriteLine("TimeSlotLists Count - " + TimeSlotLists.Count);            
        }
        //private async void PopulateTimeSlotsExternal()
        //{
        //    // Check if TimeSlotExernalLists is initated
        //    if (TimeSlotExternalLists == null)
        //        TimeSlotExternalLists = new List<TimeSlotDataModel>();

        //    var requestUrl = "https://localhost:5001/timeslots";
        //    var request = new HttpRequestMessage(HttpMethod.Get, requestUrl);
        //    //request.Headers.Add("Access-Control-Allow-Origin", "*");
        //    //request.Headers.Add("Access-Control-Allow-Credentials", "true");
        //    //request.Headers.Add("Access-Control-Allow-Headers", "Access-Control-Allow-Origin,Content-Type");
        //    HttpClient client = new HttpClient();            
        //    var httpResponse = await client.SendAsync(request);
        //    if (httpResponse.StatusCode == System.Net.HttpStatusCode.OK)
        //    {
        //        Console.WriteLine("Response OK");
        //        var httpResponseContentString = await httpResponse.Content.ReadAsStringAsync();
        //        Console.WriteLine(httpResponseContentString);
        //        TimeSlotExternalLists = JsonConvert.DeserializeObject<List<TimeSlotDataModel>>(httpResponseContentString);
        //        Console.WriteLine(JsonConvert.SerializeObject(TimeSlotExternalLists, Formatting.Indented));
        //    }
        //    else
        //    {
        //        Console.WriteLine("Response Error - " + httpResponse.StatusCode.ToString());
        //    }
        //    Console.WriteLine("TimeSlotExternalLists Count - " + TimeSlotExternalLists.Count);
        //}
        //private void PopulateSettings()
        //{
            
        //}



        //private void PopulateRelaysMocked()
        //{
        //    // Check if RelayLists is initated
        //    if (RelayLists == null)
        //        RelayLists = new List<RelayDataModel>();

        //    RelayDataModel relay01 = new RelayDataModel()         // Court 01 - A
        //    {
        //        Id = "01",
        //        State = RelayDataModel.RelayState.On
        //    };
        //    RelayLists.Add(relay01);

        //    RelayDataModel relay02 = new RelayDataModel()         // Court 01 - B
        //    {
        //        Id = "02",
        //        State = RelayDataModel.RelayState.On
        //    };
        //    RelayLists.Add(relay02);

        //    RelayDataModel relay03 = new RelayDataModel()         // Court 01 - C
        //    {
        //        Id = "03",
        //        State = RelayDataModel.RelayState.On
        //    };
        //    RelayLists.Add(relay03);

        //    RelayDataModel relay04 = new RelayDataModel()         // Court 02 - A
        //    {
        //        Id = "04",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay04);

        //    RelayDataModel relay05 = new RelayDataModel()         // Court 02 - B
        //    {
        //        Id = "05",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay05);

        //    RelayDataModel relay06 = new RelayDataModel()         // Court 02 - C
        //    {
        //        Id = "06",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay06);

        //    RelayDataModel relay07 = new RelayDataModel()         // Court 03 - A
        //    {
        //        Id = "07",
        //        State = RelayDataModel.RelayState.On
        //    };
        //    RelayLists.Add(relay07);

        //    RelayDataModel relay08 = new RelayDataModel()         // Court 03 - B
        //    {
        //        Id = "08",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay08);

        //    RelayDataModel relay09 = new RelayDataModel()         // Court 03 - C
        //    {
        //        Id = "09",
        //        State = RelayDataModel.RelayState.On
        //    };
        //    RelayLists.Add(relay09);

        //    RelayDataModel relay10 = new RelayDataModel()         // Court 04 - A
        //    {
        //        Id = "10",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay10);

        //    RelayDataModel relay11 = new RelayDataModel()         // Court 04 - B
        //    {
        //        Id = "11",
        //        State = RelayDataModel.RelayState.On
        //    };
        //    RelayLists.Add(relay11);

        //    RelayDataModel relay12 = new RelayDataModel()         // Court 04 - C
        //    {
        //        Id = "12",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay12);

        //    RelayDataModel relay13 = new RelayDataModel()         // NOT USED
        //    {
        //        Id = "13",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay13);

        //    RelayDataModel relay14 = new RelayDataModel()         // NOT USED
        //    {
        //        Id = "14",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay14);

        //    RelayDataModel relay15 = new RelayDataModel()         // NOT USED
        //    {
        //        Id = "15",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay15);

        //    RelayDataModel relay16 = new RelayDataModel()         // NOT USED
        //    {
        //        Id = "16",
        //        State = RelayDataModel.RelayState.Off
        //    };
        //    RelayLists.Add(relay16);
        //}
        //private void PopulateTimeSlotsMocked()
        //{
        //    // Check if TimeSlotLists is initated
        //    if (TimeSlotLists == null)
        //        TimeSlotLists = new List<TimeSlotDataModel>();

        //    // MOCK DATA PLAN - 3 DAYS - (XmasEve - 24th / XmasDay - 25th / BoxingDay - 26th)

        //    TimeSlotDataModel ts20201224s01 = new TimeSlotDataModel()
        //    {
        //        Id = "2020-0123",
        //        CreatedOn = new DateTime(2020, 12, 20, 09, 33, 59),
        //        CreatedByUserId = "12341234",
        //        CreatedByUsername = "Kurt Carabott",
        //        Type = TimeSlotMode.tstExternal,
        //        State = TimeSlotDataModel.TimeSlotStateDataModel.On,
        //        Title = "Title Text 24 - 01",
        //        Description = "Description longer Text 24 - 01",
        //        TimeFrom = new DateTime(2020, 12, 24, 13, 00, 00),
        //        TimeTo = new DateTime(2020, 12, 24, 14, 30, 00),
        //        RelayGroupId = RelayGroup.RG_01,
        //        SunlightAwareAffected = false
        //    };
        //    TimeSlotLists.Add(ts20201224s01);

        //    TimeSlotDataModel ts20201224s02 = new TimeSlotDataModel()
        //    {
        //        Id = "2020-0124",
        //        CreatedOn = new DateTime(2020, 12, 20, 09, 37, 03),
        //        CreatedByUserId = "12342345",
        //        CreatedByUsername = "Joeseph D.",
        //        Type = TimeSlotMode.tstExternal,
        //        State = TimeSlotDataModel.TimeSlotStateDataModel.On,
        //        Title = "Title Text 24 - 02",
        //        Description = "Description longer Text 24 - 02",
        //        TimeFrom = new DateTime(2020, 12, 24, 14, 30, 00),
        //        TimeTo = new DateTime(2020, 12, 24, 16, 00, 00),
        //        RelayGroupId = RelayGroup.RG_01,
        //        SunlightAwareAffected = false
        //    };
        //    TimeSlotLists.Add(ts20201224s02);

        //    TimeSlotDataModel ts20201224s03 = new TimeSlotDataModel()
        //    {
        //        Id = "2020-0125",
        //        CreatedOn = new DateTime(2020, 12, 20, 09, 33, 59),
        //        CreatedByUserId = "12341234",
        //        CreatedByUsername = "Kurt Carabott",
        //        Type = TimeSlotMode.tstExternal,
        //        State = TimeSlotDataModel.TimeSlotStateDataModel.On,
        //        Title = "Title Text 24 - 03",
        //        Description = "Description longer Text 24 - 03",
        //        TimeFrom = new DateTime(2020, 12, 24, 17, 30, 00),
        //        TimeTo = new DateTime(2020, 12, 24, 19, 00, 00),
        //        RelayGroupId = RelayGroup.RG_01,
        //        SunlightAwareAffected = false
        //    };
        //    TimeSlotLists.Add(ts20201224s03);
        //}
        //private void PopulateSettingsMocked()
        //{
        //    if (Settings == null)
        //        Settings = new SettingDataModel();

        //    SettingDataModel setting = new SettingDataModel()
        //    {
        //        Latitude = 35.917973,
        //        Longitude = 14.409943,
        //        SunlightAware = true,
        //        AdvancedSetings = new AdvancedSettingDataModel()
        //        {
        //            LogLevel = LogLevelType.lltWarning,
        //            KeepAgendaHistoryForDays = "10",
        //            RunCleanJobEveryDays = "1"
        //        },
        //        RelayGroupsSettings = new RelayGroupsSettingDataModel()
        //        {
        //            RG_01 = new GroupSettingsDataModel()
        //            {
        //                Mode = TimeSlotMode.tstInternal,
        //                SunlightOffset = 0
        //            },
        //            RG_02 = new GroupSettingsDataModel()
        //            {
        //                Mode = TimeSlotMode.tstExternal,
        //                SunlightOffset = 0
        //            },
        //            RG_03 = new GroupSettingsDataModel()
        //            {
        //                Mode = TimeSlotMode.tstInternal,
        //                SunlightOffset = 0
        //            },
        //            RG_04 = new GroupSettingsDataModel()
        //            {
        //                Mode = TimeSlotMode.tstExternal,
        //                SunlightOffset = 0
        //            }
        //        }
        //    };

        //    Settings = setting;
        //}

        //private void TryGetConfiguration(string method)
        //{
        //    if (Configuration != null)
        //    {
        //        string env = Configuration["ENVIRONMENT_MODE"];
        //        Console.WriteLine("Tried to get Config: ENVIRONMENT_MODE = {" + env + "} - from " + method);
        //    }
        //    else
        //    {
        //        Console.WriteLine("Tried to get Config: Config is NULL - from " + method);
        //    }
        //}


    }
}
