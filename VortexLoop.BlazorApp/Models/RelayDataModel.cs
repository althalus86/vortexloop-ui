using Newtonsoft.Json;
using System;
using System.Text.Json.Serialization;

namespace VortexLoop.BlazorApp.Models
{
    // Relay Object Info
    public class RelayDataModel
    {
        public enum RelayState
        {
            Off = 0,
            On = 1
        }

        [JsonProperty("id")]
        [JsonPropertyName("id")]
        public int Id { get; set; }                     // Number 1-16        
        [JsonProperty("state")]
        [JsonPropertyName("state")]
        public RelayState State { get; set; }           // ON = 1 | OFF = 0              
        [JsonProperty("relayGroupId")]
        [JsonPropertyName("relayGroupId")]
        public string RelayGroupId { get; set; }        // Crt01 = "1" | Crt02 = "2" | Crt03 = "3" | Crt04 = "4"  
    }
}
