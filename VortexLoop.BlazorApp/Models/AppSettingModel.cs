﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VortexLoop.BlazorApp.Models
{
    public class AppSettingModel
    {
        public const string ENVIRONMENT_MODE_KEY = "ENVIRONMENT_MODE";
        public const string ENVIRONMENT_APP_BASE_URL_KEY = "ENVIRONMENT_APP_BASE_URL";
        public const string ENVIRONMENT_API_BASE_URL_KEY = "ENVIRONMENT_API_BASE_URL";
        public const string ENVIRONMENT_API_PROXY_BASE_URL_KEY = "ENVIRONMENT_API_PROXY_BASE_URL";
        public const string RELAY_RESOURCE_ROUTE_KEY = "RELAY_RESOURCE_ROUTE";
        public const string RELAY_USE_PROXY_KEY = "RELAY_USE_PROXY";
        public const string RELAY_USE_MOCK_KEY = "RELAY_USE_MOCK";
        public const string TIMESLOT_RESOURCE_ROUTE_KEY = "TIMESLOT_RESOURCE_ROUTE";
        public const string TIMESLOT_USE_PROXY_KEY = "TIMESLOT_USE_PROXY";
        public const string TIMESLOT_USE_MOCK_KEY = "TIMESLOT_USE_MOCK";
        public const string SETTING_RESOURCE_ROUTE_KEY = "SETTING_RESOURCE_ROUTE";
        public const string SETTING_USE_PROXY_KEY = "SETTING_USE_PROXY";
        public const string SETTING_USE_MOCK_KEY = "SETTING_USE_MOCK";
        public const string SYNCFUSION_KEY_KEY = "SYNCFUSION_KEY";
        public const string APP_VERSION_KEY = "APP_VERSION";

        public string ENVIRONMENT_MODE { get; set; }
        public string ENVIRONMENT_APP_BASE_URL { get; set; }
        public string ENVIRONMENT_API_BASE_URL { get; set; }        
        public string ENVIRONMENT_API_PROXY_BASE_URL { get; set; }

        public string RELAY_RESOURCE_ROUTE { get; set; }
        public string RELAY_USE_PROXY { get; set; }
        public string RELAY_USE_MOCK { get; set; }

        public string TIMESLOT_RESOURCE_ROUTE { get; set; }
        public string TIMESLOT_USE_PROXY { get; set; }
        public string TIMESLOT_USE_MOCK { get; set; }

        public string SETTING_RESOURCE_ROUTE { get; set; }
        public string SETTING_USE_PROXY { get; set; }
        public string SETTING_USE_MOCK { get; set; }

        public string SYNCFUSION_KEY { get; set; }

        public string APP_VERSION { get; set; }

    }
}
