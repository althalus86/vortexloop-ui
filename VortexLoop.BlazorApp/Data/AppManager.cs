﻿using Innovative.SolarCalculator;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Data
{
    public class AppManager
    {
        [Inject] public NavigationManager NavigationManager { get; set; }
        [Inject] public HttpClient HttpClient { get; set; }
        [Inject] public IAccessTokenProvider TokenProvider { get; set; }

        // DataModels
        private AppSettingModel AppSettings { get; set; }
        private List<RelayDataModel> RelayList { get; set; }
        private List<TimeSlotDataModel> TimeSlotList { get; set; }
        private SettingDataModel Settings { get; set; }

        // SECURITY
        private string AppAccessTokenValue { get; set; }

        public AppManager()
        {            
            //Loggy.Log(Loggy.LogType.Debug, "CONSTRUCTOR AppManager");

            RelayList = new List<RelayDataModel>();
            TimeSlotList = new List<TimeSlotDataModel>();
            AppSettings = new AppSettingModel();            
        }             

        public void InitializeAppManager(AppSettingModel appSettings, HttpClient httpClient)
        {            
            //Loggy.Log(Loggy.LogType.Debug, "[AppManager] INITIALISE AppManager");
            SetAppSettings(appSettings);
            HttpClient = httpClient;
            //NavigationManagerInstance = navigationManager;

            //Loggy.Log(Loggy.LogType.Debug, $"[AppManager] Environement Mode = {AppSettings.ENVIRONMENT_MODE}");

            //Loggy.Log(Loggy.LogType.Debug, $"[AppManager] HttpClient = {HttpClient}");
            //Loggy.Log(Loggy.LogType.Debug, $"[AppManager] TokenProvider = {TokenProvider}");
            //Loggy.Log(Loggy.LogType.Debug, $"[AppManager] NavigationManager = {NavigationManager}");

        }

        // ---------------------------------------------------------------------
        // START OF SINGLTON CALLS

        // CONFIGURATION METHODS
        private void SetAppSettings(AppSettingModel appSettings)
        {
            try
            {
                if (appSettings != null)
                {
                    AppSettings = appSettings;
                }
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}  | Stack Trace = {ex.StackTrace}");
                throw;
            }            
        }
        public AppSettingModel GetAppSettings() => AppSettings;

        // RELAY METHODS
        public List<RelayDataModel> GetRelayList()
        {
            try
            {
                return RelayList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public List<RelayDataModel> UpdateRelayList(List<RelayDataModel> newRelayList)
        {
            try
            {
                RelayList = newRelayList;
                return RelayList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public List<RelayDataModel> UpdateRelayList(RelayDataModel updatedRelay)
        {
            try
            {
                List<RelayDataModel> newRelayList = RelayList;
                for (int index = 0; index < newRelayList.Count-1; index++)
                {
                    if (newRelayList[index].Id == updatedRelay.Id)
                    {
                        newRelayList[index].State = updatedRelay.State;
                        newRelayList[index].RelayGroupId = updatedRelay.RelayGroupId;
                    }
                }
                RelayList = newRelayList;
                return RelayList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // TIMESLOT METHODS
        public List<TimeSlotDataModel> GetTimeslotList()
        {
            try
            {
                return TimeSlotList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public List<TimeSlotDataModel> UpdateTimeslotList(List<TimeSlotDataModel> newTimeslotList)
        {
            try
            {
                TimeSlotList = newTimeslotList;
                return TimeSlotList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public List<TimeSlotDataModel> UpdateTimeslotList(TimeSlotDataModel updatedRelay)
        {
            try
            {
                List<TimeSlotDataModel> newTimeslotList = TimeSlotList;
                for (int index = 0; index < newTimeslotList.Count - 1; index++)
                {
                    if (newTimeslotList[index].Id == updatedRelay.Id)
                    {
                        newTimeslotList[index].CreatedOn = updatedRelay.CreatedOn;
                        newTimeslotList[index].CreatedByUserId = updatedRelay.CreatedByUserId;
                        newTimeslotList[index].CreatedByUsername = updatedRelay.CreatedByUsername;
                        newTimeslotList[index].Type = updatedRelay.Type;
                        newTimeslotList[index].State = updatedRelay.State;
                        newTimeslotList[index].Title = updatedRelay.Title;
                        newTimeslotList[index].Description = updatedRelay.Description;
                        newTimeslotList[index].TimeFrom = updatedRelay.TimeFrom;
                        newTimeslotList[index].TimeTo = updatedRelay.TimeTo;
                        newTimeslotList[index].OriginalTimeFrom = updatedRelay.OriginalTimeFrom;
                        newTimeslotList[index].OriginalTimeTo = updatedRelay.OriginalTimeTo;
                        newTimeslotList[index].RelayGroupId = updatedRelay.RelayGroupId;
                        newTimeslotList[index].SunlightAwareAffected = updatedRelay.SunlightAwareAffected;
                    }
                }
                TimeSlotList = newTimeslotList;
                return TimeSlotList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // RELAY METHODS
        public SettingDataModel GetSettings()
        {
            try
            {
                return Settings;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public SettingDataModel UpdateSettings(SettingDataModel newSettings)
        {
            try
            {
                Settings = newSettings;
                return Settings;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // ENG OF SINGLTON CALLS
        // ---------------------------------------------------------------------
        // START OF HTTP CALLS

        // RELAY METHODS
        public async Task<List<RelayDataModel>> HttpGetRelays(string tokenValue, bool ReFetch = true)
        {
            try
            {
                var latestRelayList = GetRelayList();
                if (ReFetch == false && latestRelayList != null)
                    return latestRelayList;
                else
                {
                    HttpResponseMessage httpResponseMessage = await SecureApiCall(tokenValue, HttpMethod.Get, GetAppSettings().RELAY_RESOURCE_ROUTE);
                    //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                    if (httpResponseMessage != null)
                    {
                        //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                        if (httpResponseMessage.StatusCode == HttpStatusCode.OK || httpResponseMessage.StatusCode == HttpStatusCode.Accepted)
                        {
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                            var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                            if (!String.IsNullOrEmpty(responseBody))
                            {
                                //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                                List<RelayDataModel> freshRelayList = new List<RelayDataModel>();
                                //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                                freshRelayList = JsonSerializer.Deserialize<List<RelayDataModel>>(responseBody);
                                //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshRelayList);
                                if (freshRelayList != null && freshRelayList.Count > 0)
                                {
                                    //Loggy.Log(Loggy.LogType.Debug, $"Fresh Relay List (Not Null) Count = " + freshRelayList.Count);
                                    latestRelayList = UpdateRelayList(freshRelayList);
                                }
                            }
                            else
                            {
                                Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: Response Body is Null or Empty : ResponseStatus" + httpResponseMessage.StatusCode.ToString() + " | ResponseBody=" + responseBody);
                                
                            }
                        }
                        else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);

                        }
                        else
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                            throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                        }
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = HttpResponse = Null - Something wend wrong");
                        throw new Exception("HttpResponse=Null - Something wend wrong");
                    }

                    return latestRelayList;
                }

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public async Task<List<RelayDataModel>> HttpUpdateRelay(string tokenValue, int id, int state)
        {
            var updatedRelayList = await HttpUpdateRelay(tokenValue, id, (RelayDataModel.RelayState)state);
            return updatedRelayList;
        }
        public async Task<List<RelayDataModel>> HttpUpdateRelay(string tokenValue, int id, RelayDataModel.RelayState state)
        {
            try
            {
                var latestRelayList = GetRelayList();

                //Loggy.Log(Loggy.LogType.Debug, $"Creating new Relay Model to Update Relay with ID = {id} and State = {state}");


                RelayDataModel newRelayDataModel = GetRelayList().Find(item => item.Id == id);
                newRelayDataModel.State = state;
                string jsonBody = JsonSerializer.Serialize(newRelayDataModel);
                //Loggy.Log(Loggy.LogType.Debug, $"New Json Body String = {jsonBody}");

                HttpResponseMessage httpResponseMessage = await SecureApiCall(tokenValue, HttpMethod.Put, GetAppSettings().RELAY_RESOURCE_ROUTE, jsonBody, newRelayDataModel.Id.ToString());
                //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                if (httpResponseMessage != null)
                {
                    //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK || httpResponseMessage.StatusCode == HttpStatusCode.Accepted)
                    {
                        latestRelayList = await HttpGetRelays(tokenValue, true);
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Loggy.Log(Loggy.LogType.Warning, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                                                     " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                                                     " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                                                     " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                                                     " | Object=" + httpResponseMessage);
                        latestRelayList = null;
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Loggy.Log(Loggy.LogType.Warning, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                                                     " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                                                     " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                                                     " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                                                     " | Object=" + httpResponseMessage);
                        latestRelayList = null;
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                             " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                             " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                             " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                             " | Object=" + httpResponseMessage);
                        throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                    }
                }
                else
                {
                    Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                                                 " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                                                 " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                                                 " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                                                 " | Object=" + httpResponseMessage);
                    throw new Exception("HttpResponse=Null - Something wend wrong");
                }

                return latestRelayList;
            }                        
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // TIMESLOT METHODS
        public async Task<List<TimeSlotDataModel>> HttpGetTimeslots(string tokenValue, bool ReFetch = true)
        {
            try
            {
                var latestTimeslotList = GetTimeslotList();
                if (ReFetch == false && latestTimeslotList != null)
                    return latestTimeslotList;
                else
                {
                    HttpResponseMessage httpResponseMessage = await SecureApiCall(tokenValue, HttpMethod.Get, GetAppSettings().TIMESLOT_RESOURCE_ROUTE);
                    //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                    if (httpResponseMessage != null)
                    {
                        //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                        if (httpResponseMessage.StatusCode == HttpStatusCode.OK || httpResponseMessage.StatusCode == HttpStatusCode.Accepted)
                        {
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                            var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                            if (!String.IsNullOrEmpty(responseBody))
                            {
                                //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                                List<TimeSlotDataModel> freshTimeslotList = new List<TimeSlotDataModel>();
                                //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                                freshTimeslotList = JsonSerializer.Deserialize<List<TimeSlotDataModel>>(responseBody);
                                //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshTimeslotList);
                                if (freshTimeslotList != null && freshTimeslotList.Count > 0)
                                {
                                    //Loggy.Log(Loggy.LogType.Debug, $"Fresh Relay List (Not Null) Count = " + freshTimeslotList.Count);
                                    latestTimeslotList = UpdateTimeslotList(freshTimeslotList);
                                }
                            }
                            else
                            {
                                Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: Response Body is Null or Empty : ResponseStatus" + httpResponseMessage.StatusCode.ToString() + " | ResponseBody=" + responseBody);

                            }
                        }
                        else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);

                        }
                        else
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                            throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                        }
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = HttpResponse = Null - Something wend wrong");
                        throw new Exception("HttpResponse=Null - Something wend wrong");
                    }

                    return latestTimeslotList;
                }

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public async Task<List<TimeSlotDataModel>> HttpUpdateTimeslot(string tokenValue, string id, int state)
        {
            var updatedTimeslotList = await HttpUpdateTimeslot(tokenValue, id, (TimeSlotDataModel.TimeSlotState)state);
            return updatedTimeslotList;
        }
        public async Task<List<TimeSlotDataModel>> HttpUpdateTimeslot(string tokenValue, string id, TimeSlotDataModel.TimeSlotState state)
        {
            try
            {
                var latestTimeslotList = GetTimeslotList();

                //Loggy.Log(Loggy.LogType.Debug, $"Creating new Timeslot Model to Update Timeslot with ID = {id} and State = {state}");

                TimeSlotDataModel newTimeslotDataModel = GetTimeslotList().Find(item => item.Id == id);
                newTimeslotDataModel.State = state;
                string jsonBody = JsonSerializer.Serialize(newTimeslotDataModel);
                //Loggy.Log(Loggy.LogType.Debug, $"New Json Body String = {jsonBody}");

                HttpResponseMessage httpResponseMessage = await SecureApiCall(tokenValue, HttpMethod.Put, GetAppSettings().TIMESLOT_RESOURCE_ROUTE, jsonBody, newTimeslotDataModel.Id);
                //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                if (httpResponseMessage != null)
                {
                    //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK || httpResponseMessage.StatusCode == HttpStatusCode.Accepted)
                    {
                        //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                        var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                        //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                        if (!String.IsNullOrEmpty(responseBody))
                        {
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                            List<TimeSlotDataModel> freshTimeslotList = new List<TimeSlotDataModel>();
                            //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                            freshTimeslotList = JsonSerializer.Deserialize<List<TimeSlotDataModel>>(responseBody);
                            //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshTimeslotList);
                            if (freshTimeslotList != null && freshTimeslotList.Count > 0)
                            {
                                //Loggy.Log(Loggy.LogType.Debug, $"Fresh Timeslot List (Not Null) Count = " + freshTimeslotList.Count);
                                latestTimeslotList = UpdateTimeslotList(freshTimeslotList);
                            }
                        }
                        else
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: Response Body is Null or Empty : ResponseStatus" + httpResponseMessage.StatusCode.ToString() + " | ResponseBody=" + responseBody);                            
                        }
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);                        
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                        throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                    }
                }
                else
                {
                    Loggy.Log(Loggy.LogType.Error, $"New Exception = HttpResponse = Null - Something wend wrong");
                    throw new Exception("HttpResponse=Null - Something wend wrong");
                }

                return latestTimeslotList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // SETTING METHODS
        public async Task<SettingDataModel> HttpGetSettings(string tokenValue, bool ReFetch = true)
        {
            try
            {
                var latestSettings = GetSettings();
                if (ReFetch == false && latestSettings != null)
                    return latestSettings;
                else
                {
                    HttpResponseMessage httpResponseMessage = await SecureApiCall(tokenValue, HttpMethod.Get, GetAppSettings().SETTING_RESOURCE_ROUTE);
                    //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                    if (httpResponseMessage != null)
                    {
                        //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                        if (httpResponseMessage.StatusCode == HttpStatusCode.OK || httpResponseMessage.StatusCode == HttpStatusCode.Accepted)
                        {
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                            var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                            //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                            if (!String.IsNullOrEmpty(responseBody))
                            {
                                //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                                SettingDataModel freshSettings = new SettingDataModel();
                                //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                                freshSettings = JsonSerializer.Deserialize<SettingDataModel>(responseBody);
                                //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshSettings);
                                if (freshSettings != null)
                                {
                                    //Loggy.Log(Loggy.LogType.Debug, $"Fresh Settings List (Not Null) Count = " + freshSettings.Latitude);
                                    latestSettings = UpdateSettings(freshSettings);

                                    var prettyJsonSettings = JsonSerializer.Serialize<SettingDataModel>(latestSettings, new JsonSerializerOptions() { WriteIndented = true });
                                    //Loggy.Log(Loggy.LogType.Debug, $"Fresh Setings Pretty = " + Environment.NewLine + prettyJsonSettings);
                                }
                            }
                            else
                            {
                                Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: Response Body is Null or Empty : ResponseStatus" + httpResponseMessage.StatusCode.ToString() + " | ResponseBody=" + responseBody);
                                
                            }
                        }
                        else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);

                        }
                        else
                        {
                            Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                            throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                        }
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = HttpResponse = Null - Something wend wrong");
                        throw new Exception("HttpResponse=Null - Something wend wrong");
                    }

                    return latestSettings;
                }

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        public async Task<SettingDataModel> HttpUpdateSettingsGroupMode(string tokenValue, string relayGroupId, string newRelayGroupMode)
        {
            try
            {
                var latestSettings = GetSettings();


                GroupSettingsDataModel_RelayGroupModeFocus newGroupSettingsDataModel_RelayGroupModeFocus = new GroupSettingsDataModel_RelayGroupModeFocus();
                newGroupSettingsDataModel_RelayGroupModeFocus.Mode = newRelayGroupMode;
                
                SettingDataModel_RelayGroupModeFocus newSettingDataModel_RelayGroupModeFocus = new SettingDataModel_RelayGroupModeFocus();
                newSettingDataModel_RelayGroupModeFocus.GroupSettings_RelayGroupModeFocus = new Dictionary<string, GroupSettingsDataModel_RelayGroupModeFocus>();
                
                newSettingDataModel_RelayGroupModeFocus.GroupSettings_RelayGroupModeFocus.Add(relayGroupId, newGroupSettingsDataModel_RelayGroupModeFocus);
                
                string jsonBody = JsonSerializer.Serialize(newSettingDataModel_RelayGroupModeFocus);

                Loggy.Log(Loggy.LogType.Debug, $"New HttpUpdateSettingsGroupMode Json Body String = {jsonBody}");

                HttpResponseMessage httpResponseMessage = await SecureApiCall(tokenValue, HttpMethod.Patch, GetAppSettings().SETTING_RESOURCE_ROUTE, jsonBody);
                Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);

                if (httpResponseMessage != null)
                {
                    //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK || httpResponseMessage.StatusCode == HttpStatusCode.Accepted || httpResponseMessage.StatusCode == HttpStatusCode.NoContent)
                    {
                        latestSettings = await HttpGetSettings(tokenValue, true);
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.BadRequest)
                    {
                        Loggy.Log(Loggy.LogType.Warning, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                                                     " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                                                     " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                                                     " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                                                     " | Object=" + httpResponseMessage);
                        latestSettings = null;
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        Loggy.Log(Loggy.LogType.Warning, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                                                     " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                                                     " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                                                     " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                                                     " | Object=" + httpResponseMessage);
                        latestSettings = null;
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                             " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                             " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                             " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                             " | Object=" + httpResponseMessage);
                        throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase + " | Object=" + httpResponseMessage);
                    }
                }
                else
                {
                    Loggy.Log(Loggy.LogType.Error, $"New Exception = " + "SecureApiCall Error: " + Environment.NewLine +
                                                 " | HttpCode=" + httpResponseMessage.StatusCode.ToString() + Environment.NewLine +
                                                 " | ReasonPhrase=" + httpResponseMessage.ReasonPhrase + Environment.NewLine +
                                                 " | Message=" + await httpResponseMessage.Content.ReadAsStringAsync() + Environment.NewLine +
                                                 " | Object=" + httpResponseMessage);
                    throw new Exception("HttpResponse=Null - Something wend wrong");
                }

                return latestSettings;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // ENG OF HTTP CALLS
        // ---------------------------------------------------------------------
        // START OF HELPER METHODS

        // HELPER METHODS
        public async Task<HttpResponseMessage> SecureApiCall(string tokenValue, HttpMethod httpMethod, string relativePath, string jsonStringContent = "", string resourceReferenceString = "")
        {
            try
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage();

                // Setting Http Method
                httpRequestMessage.Method = httpMethod;

                // Setting URI String
                string uriString = GetAppSettings().ENVIRONMENT_API_BASE_URL + relativePath;
                uriString = (!String.IsNullOrEmpty(resourceReferenceString)) ? uriString + "/" + resourceReferenceString : uriString;
                ////Loggy.Log(Loggy.LogType.Debug, "Computed Uri String = " + uriString);
                httpRequestMessage.RequestUri = new Uri(uriString);

                // Setting Access token
                if (!String.IsNullOrEmpty(tokenValue))
                {
                    //Loggy.Log(Loggy.LogType.Debug, "AccessToken Not Null or Empty");
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", tokenValue);
                }
                else
                {
                    //Loggy.Log(Loggy.LogType.Debug, "Access Token String Null or Empty");
                    httpResponseMessage.StatusCode = HttpStatusCode.Unauthorized;
                    httpResponseMessage.ReasonPhrase = "Authentication to Remote Server Unsuccessfull. Could not get an Access Token.";
                    return httpResponseMessage;
                }

                // Adding Body Content
                ////Loggy.Log(Loggy.LogType.Debug, "Next Steps check if JsonString Content");
                if (jsonStringContent != String.Empty)
                {
                    httpRequestMessage.Content = new StringContent(jsonStringContent, Encoding.UTF8, "application/json");
                }

                // Preparing for HTTP Request
                ////Loggy.Log(Loggy.LogType.Debug, "Pre HTTP Request");
                httpResponseMessage = await HttpClient.SendAsync(httpRequestMessage);
                ////Loggy.Log(Loggy.LogType.Debug, "Post HTTP Request");

                // Outputing Response
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public string GetAppAccessTokenValue()
        {
            return AppAccessTokenValue;
        }
        public void SetAppAccessTokenValue(string newAppAccessTokenValue)
        {
            AppAccessTokenValue = newAppAccessTokenValue;
        }
        public List<string> GetRelayGroupIdList()
        {
            return new List<string>()
            {
                "1",
                "2",
                "3",
                "4"
            };
        }

        public DateTime GetNextSunriseTime(DateTimeOffset forDate, string TimeZoneString = "Europe/Berlin")
        {
            TimeZoneInfo cst = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneString);
            SolarTimes solarTimes = new SolarTimes(forDate, 35.917973, 14.409943);
            DateTime sunrise = TimeZoneInfo.ConvertTimeFromUtc(solarTimes.Sunrise.ToUniversalTime(), cst);
            return sunrise;                 
        }

        public DateTime GetNextSunsetTime(DateTimeOffset forDate, string TimeZoneString = "Europe/Berlin")
        {
            TimeZoneInfo cst = TimeZoneInfo.FindSystemTimeZoneById(TimeZoneString);
            SolarTimes solarTimes = new SolarTimes(forDate, 35.917973, 14.409943);
            DateTime sunset = TimeZoneInfo.ConvertTimeFromUtc(solarTimes.Sunset.ToUniversalTime(), cst); ;
            return sunset;
        }

        // ENG OF HELPER METHODS
        // ---------------------------------------------------------------------

    }
}
