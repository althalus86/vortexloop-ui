﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VortexLoop.BlazorApp.Data
{
    public static class Loggy
    {
        public static void Log(LogType logType, string logMessage)
        {
            string TimeStamp = DateTime.Now.ToString("yy-MM-dd HH:mm:ss.fff");
            string LogType = (logType == Loggy.LogType.Debug) ? "DEBUG"
                            : (logType == Loggy.LogType.Info) ? "INFO"
                            : (logType == Loggy.LogType.Warning) ? "WARNING"
                            : (logType == Loggy.LogType.Error) ? "ERROR"
                            : "OTHER";

            Console.WriteLine($"{TimeStamp} :: {LogType} :: {logMessage}");
        }

        public enum LogType
        {
            Debug,
            Info,
            Warning,
            Error
        }
    }
}
