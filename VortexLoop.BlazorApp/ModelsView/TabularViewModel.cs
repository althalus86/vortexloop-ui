﻿using System;
using System.Collections.Generic;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.ModelsView
{
    public class TabularVM
    {
        public List<SlotInfo> slotInfosByTime { get; set; }
        public List<SlotInfosByRelayGroup> slotInfosByRelayGroup { get; set; }        
        public DateTime ExtractAsAt { get; set; }
    }

    public class SlotInfosByRelayGroup
    {
        public string RelayGroupId { get; set; }           // RelayGroupId
        public List<SlotInfo> slotInfos { get; set; }
    }

    public class SlotInfo
    {
        public string SlotId { get; set; }
        public string SlotCourt { get; set; }           // RelayGroupId
        public string SlotCreator { get; set; }         // CreatedByUsername
        public DateTime? SlotTimeFrom { get; set; }     // OriginalTimeFrom
        public DateTime? SlotTimeTo { get; set; }       // OriginalTimeTo
        public string SlotState { get; set; }           // OriginalState
        public bool LightTimeUpdated { get; set; }      // SunlightAwareAffected
        public DateTime? LightTimeFrom { get; set; }    // UpdatedTimeFrom
        public DateTime? LightTimeTo { get; set; }      // UpdatedTimeTo
        public string LightState { get; set; }          // UpdatedState
        public string SlotType { get; set; }            // Booking OR Committee
    }   
}
