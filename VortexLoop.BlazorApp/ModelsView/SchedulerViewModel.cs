﻿using System;
using System.Collections.Generic;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.ModelsView
{
    public class SchedulerVM
    {
        public Dictionary<string, ScheduleVM> ScheduleDictionary { get; set; }
        public Dictionary<string, ComScheduleVM> ComScheduleDictionary { get; set; }
        public List<ComScheduleVM> ComScheduleList { get; set; }
        public Dictionary<string, ComScheduleRelayGroupVM> ComScheduleRelayGroupDictionary { get; set; }
        public List<ComScheduleRelayGroupVM> ComScheduleRelayGroupList { get; set; }
        public Dictionary<string, ComScheduleTypeVM> ComScheduleTypeDictionary { get; set; }
        public List<ComScheduleTypeVM> ComScheduleTypeList { get; set; }

    }
    public class ScheduleVM
    {
        public TimeSlotDataModel TimeSlot { get; set; }
        public ComScheduleVM ComSchedule { get; set; }
        public ComScheduleRelayGroupVM ComScheduleRelayGroup { get; set; }
        public ComScheduleTypeVM ComScheduleType { get; set; }
    }
    public class ComScheduleVM
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool State { get; set; }
        public bool IsReadonly { get; set; }
        public string RefRelayGroupKey { get; set; }
        public string RefTypeKey { get; set; }
    }
    public class ComScheduleRelayGroupVM
    {
        public string Key { get; set; }
        public string RelayGroupText { get; set; }
        public string RelayGroupColor { get; set; }
    }
    public class ComScheduleTypeVM
    {
        public string Key { get; set; }
        public string TypeText { get; set; }
        public string TypeColor { get; set; }
    }
}
