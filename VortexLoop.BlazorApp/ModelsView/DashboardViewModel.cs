﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VortexLoop.BlazorApp.ModelsView
{
    public class DashboardVM
    {
        public Dictionary<string, RelayGroupVM> RelayGroupList { get; set; }
        public TimeAndSunLightVM TimeAndSunLight { get; set; }
    }

    public class RelayGroupVM
    {
        public string RelayGroupId { get; set; }
        public string RelayGroupName_Text { get; set; }
        
        public string RelayA_Text { get; set; }
        public bool RelayA_Value { get; set; }
        public string RelayB_Text { get; set; }
        public bool RelayB_Value { get; set; }
        public string RelayC_Text { get; set; }
        public bool RelayC_Value { get; set; }
        
        public string RelayGroupMode_Text { get; set; }
        public string RelayGroupModeManual_Text { get; set; }       // A = Manual
        public string RelayGroupModeAutomate_Text { get; set; }     // B = Auto
        public bool IsRelayGroupModeAutomate_Value { get; set; }    // A = False | B = True        
        

        public string Automate_Text { get; set; }
        public string AutomateTypeSchedule_Text { get; set; }
        public string AutomateTypeBooking_Text { get; set; }
        public bool IsAutomateTypeBooking_Value { get; set; }
        public bool IsAutomateTypeBooking_Disabled { get; set; }

        public string ManualMode_Text { get; set; }
        public string ManualMode_BoxCssClass { get; set; }
        public bool ManualSwitchTogglesDisabled { get; set; }
        public string ManualModeA_Text { get; set; }
        public bool ManualModeA_Value { get; set; }
        public string ManualModeB_Text { get; set; }
        public bool ManualModeB_Value { get; set; }
        public string ManualModeC_Text { get; set; }
        public bool ManualModeC_Value { get; set; }

        public string AutomateMode_Text { get; set; }
        public string AutomateMode_BoxCssClass { get; set; }
        public string AutomateMode_String01 { get; set; }
        public string AutomateMode_String02 { get; set; }
        public string AutomateMode_String03 { get; set; }        
    }

    public class TimeAndSunLightVM
    {
        public string NextSunrise_Text { get; set; }
        public DateTime NextSunrise_Value { get; set; }
        public string NextSunrise_Display { get; set; }

        public string LocalDateTime_Text { get; set; }
        public DateTime LocalDateTime_Value { get; set; }
        public string LocalDateTime_Display { get; set; }

        public string NextSunset_Text { get; set; }
        public DateTime NextSunset_Value { get; set; }
        public string NextSunset_Display { get; set; }
    }
}
