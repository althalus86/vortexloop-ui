using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Syncfusion.Blazor;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            // Create Host Builder on DIV #app
            var builder = WebAssemblyHostBuilder.CreateDefault(args);            
            
            // Configuring Logging System
            builder.Logging.SetMinimumLevel(LogLevel.Information);            

            //// CONFIGURATION
            string APP_VERSION = builder.Configuration[AppSettingModel.APP_VERSION_KEY]; 
            string SYNCFUSION_KEY = builder.Configuration[AppSettingModel.SYNCFUSION_KEY_KEY]; 


            // HostEnvironment Configurations
            if (builder.HostEnvironment.IsProduction())
            {
                Loggy.Log(Loggy.LogType.Debug, $"Loading VortexLoop UI for Production Environment on App Version {APP_VERSION}"); 
            } 
            else if (builder.HostEnvironment.IsDevelopment())
            {
                Loggy.Log(Loggy.LogType.Debug, $"Loading VortexLoop UI for Development Environment on App Version {APP_VERSION}");
                //await Task.Delay(10000); 
            }

            // Root App Configuration
            builder.RootComponents.Add<App>("#app");
            // Add Blazor.Extensions.Logging.BrowserConsoleLogger
            builder.Services.AddLogging(builder => builder.SetMinimumLevel(LogLevel.Trace));
            // Add your Syncfusion license key for Blazor platform with corresponding Syncfusion NuGet version referred in project. For more information about license key see https://help.syncfusion.com/common/essential-studio/licensing/license-key.            
            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense(SYNCFUSION_KEY);
            

            //// DEPENDENCY INJECTION

            // Add HttpClient
            builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

            // Adding OIDC Authentication
            builder.Services.AddOidcAuthentication(options =>
            {
                // Configure your authentication provider options here.
                // For more information, see https://aka.ms/blazor-standalone-auth
                builder.Configuration.Bind("Local", options.ProviderOptions);
                options.ProviderOptions.ResponseType = "code";
                options.ProviderOptions.DefaultScopes.Add("profile");
            });

            // Adding SyncFusion Components Config
            builder.Services.AddSyncfusionBlazor();

            // Add Singlton Daa Service
            builder.Services.AddSingleton<AppManager>();

            // Build Host
            var host = builder.Build();

            // Initialize Setup
            var httpClient = host.Services.GetRequiredService<HttpClient>();
            var AppManager = host.Services.GetRequiredService<AppManager>();            

            AppSettingModel appSettings = GetAppSettings(builder.Configuration);
            AppManager.InitializeAppManager(appSettings, httpClient);
            

            // Run the Host Builder
            await host.RunAsync();
        }

        private static AppSettingModel GetAppSettings(WebAssemblyHostConfiguration Configuration)
        {
            AppSettingModel AppSettings = new AppSettingModel();
            if (Configuration != null)
            {
                AppSettings.ENVIRONMENT_MODE = Configuration[AppSettingModel.ENVIRONMENT_MODE_KEY];
                AppSettings.ENVIRONMENT_APP_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_APP_BASE_URL_KEY];
                AppSettings.ENVIRONMENT_API_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_API_BASE_URL_KEY];
                AppSettings.ENVIRONMENT_API_PROXY_BASE_URL = Configuration[AppSettingModel.ENVIRONMENT_API_PROXY_BASE_URL_KEY];
                AppSettings.RELAY_RESOURCE_ROUTE = Configuration[AppSettingModel.RELAY_RESOURCE_ROUTE_KEY];
                AppSettings.RELAY_USE_PROXY = Configuration[AppSettingModel.RELAY_USE_PROXY_KEY];
                AppSettings.RELAY_USE_MOCK = Configuration[AppSettingModel.RELAY_USE_MOCK_KEY];
                AppSettings.TIMESLOT_RESOURCE_ROUTE = Configuration[AppSettingModel.TIMESLOT_RESOURCE_ROUTE_KEY];
                AppSettings.TIMESLOT_USE_PROXY = Configuration[AppSettingModel.TIMESLOT_USE_PROXY_KEY];
                AppSettings.TIMESLOT_USE_MOCK = Configuration[AppSettingModel.TIMESLOT_USE_MOCK_KEY];
                AppSettings.SETTING_RESOURCE_ROUTE = Configuration[AppSettingModel.SETTING_RESOURCE_ROUTE_KEY];
                AppSettings.SETTING_USE_PROXY = Configuration[AppSettingModel.SETTING_USE_PROXY_KEY];
                AppSettings.SETTING_USE_MOCK = Configuration[AppSettingModel.SETTING_USE_MOCK_KEY];
            }
            return AppSettings;
        }

    }
}
