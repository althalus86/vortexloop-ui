﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Syncfusion.Blazor.Buttons;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;
using VortexLoop.BlazorApp.ModelsView;

namespace VortexLoop.BlazorApp.Pages
{
    public partial class Dashboard
    {
        [CascadingParameter] Task<AuthenticationState> _AuthenticationState { get; set; }
        [Inject] NavigationManager _NavigationManager { get; set; }
        [Inject] HttpClient _HttpClient { get; set; }
        [Inject] IAccessTokenProvider _TokenProvider { get; set; }
        [Inject] SignOutSessionStateManager _SignOutManager { get; set; }
        [Inject] AppManager _AppManager { get; set; }

        public DashboardVM _DashboardVM { get; set; }

        // Component References        
        private Dictionary<string, SfSwitch<bool>> ManualSwitchToggles = new Dictionary<string, SfSwitch<bool>>();

        // Blazor LifeCycle Trigger 00
        public Dashboard()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] Dashboard() = BLT00");
            try
            {          
                //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        //Blazor LifeCycle Trigger 01
        //public override async Task SetParametersAsync(ParameterView parameters)
        //{
        //    Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] SetParametersAsync(ParameterView parameters) - BLT01");
        //    try
        //    {
        //        Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard] ParameterView = {parameters.ToString()}");
        //        Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].SetParametersAsync(ParameterView parameters) - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}

        // Blazor LifeCycle Trigger 02
        protected override void OnInitialized()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] OnInitialized() - BLT02");
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard].OnInitialized() - AppManager = {_AppManager}");
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard].OnInitialized() - HttpClient = {_HttpClient}");
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard].OnInitialized() - TokenProvider = {_TokenProvider}");
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard].OnInitialized() - NavigationManager = {_NavigationManager}");

                _DashboardVM = GetInitialDashboardVM();

                //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnInitialized() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 03
        protected override async Task OnInitializedAsync()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] OnInitializedAsync() - BLT03");
            try
            {
                _DashboardVM = await GetUpdatedDashboardData();                

                //await InvokeAsync(() =>
                //{
                //    StateHasChanged();
                //});

              //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnInitializedAsync() - {ManualSwitchToggles["22"].Checked} - {_DashboardVM.RelayGroupList["2"].ManualModeB_Value}");
              //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnInitializedAsync() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 04 & 06
        //protected override async Task OnAfterRenderAsync(bool firstRender)
        //{
        //    //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] OnAfterRenderAsync(bool firstRender) - BLT04_BLT06");
        //    try
        //    {
        //        //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard] firstRender = {firstRender.ToString()}");                
        //        if (firstRender)
        //        {
        //          //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnAfterRenderAsync(bool firstRender) - FIRST RENDER - BLT04");
        //        }
        //        else
        //        {
        //          //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnAfterRenderAsync(bool firstRender) - NEXT RENDER - BLT06");
        //        }
        //        //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnInitializedAsync() - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}


        // Blazor LifeCycle Trigger 05
        protected override void OnParametersSet()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] OnParametersSet() - BLT05"); 
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnParametersSet() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // -----------------------------------------------------------
        // Frontend Events

        // Court Mode
        protected async Task Court1ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Court1ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("1", args.Checked, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task Court2ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Court2ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("2", args.Checked, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task Court3ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Court3ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("3", args.Checked, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task Court4ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Court4ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("4", args.Checked, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Automate Mode
        protected async Task Automate1ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Automate1ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("1", args.Checked, false, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task Automate2ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Automate2ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("2", args.Checked, false, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task Automate3ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Automate3ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("3", args.Checked, false, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task Automate4ModeSwitchValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().Automate4ModeSwitchValueChanged - {args.Checked}");
                await HandleSettingModeChange("4", args.Checked, false, true);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Manual Court Light Toggles
        protected async Task ManualSwitch11ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch11ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("11", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }                       
        }
        protected async Task ManualSwitch21ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch21ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("21", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }            
        }
        protected async Task ManualSwitch31ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch31ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("31", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch41ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch41ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("41", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch12ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch12ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("12", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch22ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {               
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch22ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("22", args.Checked);                
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch32ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch32ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("32", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch42ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch42ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("42", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch13ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch13ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("13", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch23ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch23ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("23", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch33ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch33ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("33", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        protected async Task ManualSwitch43ValueChanged(ChangeEventArgs<bool> args)
        {
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().ManualSwitch43ValueChanged - {args.Checked}");
                await HandleManualSwitchChange("43", args.Checked);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }    

        // -----------------------------------------------------------
        // Dashboard Methods

        private DashboardVM GetInitialDashboardVM()
        {
          //Loggy.Log(Loggy.LogType.Debug, "[Dashboard] Entered GetInitialDashboardVM()");
            try
            {
                const string RelayGroupNameRef = "Court";

                DashboardVM dashboardVM = new DashboardVM();            
                Dictionary<string, RelayGroupVM> relayGroupList = new Dictionary<string, RelayGroupVM>();            
                var relayGroupIdList = _AppManager.GetRelayGroupIdList();
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] relayGroupIdList = {relayGroupIdList}");            
                foreach (var relayGroupId in relayGroupIdList)
                {
                  //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] relayGroupId = {relayGroupId}");
                    RelayGroupVM courtVM = new RelayGroupVM();
                
                    courtVM.RelayGroupId = $"0{relayGroupId}";
                    courtVM.RelayGroupName_Text = $"{RelayGroupNameRef} 0{relayGroupId}";

                    courtVM.RelayA_Text = $"Bulb {relayGroupId}1";
                    courtVM.RelayA_Value = false;
                    courtVM.RelayB_Text = $"Bulb {relayGroupId}2";
                    courtVM.RelayB_Value = false;
                    courtVM.RelayC_Text = $"Bulb {relayGroupId}3";
                    courtVM.RelayC_Value = false;

                    courtVM.RelayGroupMode_Text = $"{RelayGroupNameRef} Mode:";
                    courtVM.RelayGroupModeManual_Text = $"Manual";
                    courtVM.RelayGroupModeAutomate_Text = $"Automate";
                    courtVM.IsRelayGroupModeAutomate_Value = true;                    

                    courtVM.Automate_Text = $"Automate Mode:";
                    courtVM.AutomateTypeSchedule_Text = $"Committee";
                    courtVM.AutomateTypeBooking_Text = $"Bookings";
                    courtVM.IsAutomateTypeBooking_Value = true;
                    courtVM.IsAutomateTypeBooking_Disabled = false;

                    courtVM.ManualMode_Text = $"Manual Mode";
                    courtVM.ManualMode_BoxCssClass = $"boxInActive";
                    courtVM.ManualSwitchTogglesDisabled = true;
                    courtVM.ManualModeA_Text = $"{relayGroupId}1";
                    courtVM.ManualModeA_Value = false;
                    courtVM.ManualModeB_Text = $"{relayGroupId}2";
                    courtVM.ManualModeB_Value = false;
                    courtVM.ManualModeC_Text = $"{relayGroupId}3";
                    courtVM.ManualModeC_Value = false;

                    string AutomateType = (courtVM.IsAutomateTypeBooking_Value) ? "Bookings" : "Committee";
                    courtVM.AutomateMode_Text = $"{AutomateType} Mode";
                    courtVM.AutomateMode_BoxCssClass = $"boxActive";
                    courtVM.AutomateMode_String01 = $"Next Lit Slot:";
                    courtVM.AutomateMode_String02 = $"Today 20:30 - 22:00";
                    courtVM.AutomateMode_String03 = $"By: John Doe";

                    relayGroupList.Add(relayGroupId, courtVM);
                }
                dashboardVM.RelayGroupList = relayGroupList;

                var curDT = DateTime.Now;
                var nextSunrise = _AppManager.GetNextSunriseTime(curDT);
                var nextSunset = _AppManager.GetNextSunsetTime(curDT);
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Sunrise = {nextSunrise} / Sunset = {nextSunset}");

                TimeAndSunLightVM timeAndSunLightVM = new TimeAndSunLightVM();
                timeAndSunLightVM.NextSunrise_Text = $"Sunrise:";
                timeAndSunLightVM.NextSunrise_Value = nextSunrise;
                timeAndSunLightVM.NextSunrise_Display = $"{timeAndSunLightVM.NextSunrise_Value.ToShortTimeString()}";
                timeAndSunLightVM.LocalDateTime_Text = $"Date & Time:";
                timeAndSunLightVM.LocalDateTime_Value = curDT;
                timeAndSunLightVM.LocalDateTime_Display = curDT.ToString("ddd, dd MMM HH:mm:ss");
                timeAndSunLightVM.NextSunset_Text = $"Sunset:";
                timeAndSunLightVM.NextSunset_Value = nextSunset;
                timeAndSunLightVM.NextSunset_Display = $"{timeAndSunLightVM.NextSunset_Value.ToShortTimeString()}";
                dashboardVM.TimeAndSunLight = timeAndSunLightVM;            

                return dashboardVM;

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        private async Task<DashboardVM> GetUpdatedDashboardData()
        {
            Loggy.Log(Loggy.LogType.Debug, "[Dashboard] Entered GetUpdatedDashboardData()");
            try
            {

                DashboardVM curDashboardVM = new DashboardVM();
                curDashboardVM = _DashboardVM;

                var tokenValue = await GetAccessTokenValue();
              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Check Token Value = " + tokenValue);

                var latestSettings = new SettingDataModel();
                latestSettings = await _AppManager.HttpGetSettings(tokenValue);

                var latestRelays = new List<RelayDataModel>();
                latestRelays = await _AppManager.HttpGetRelays(tokenValue);

                var latestTimeslots = new List<TimeSlotDataModel>();
                latestTimeslots = await _AppManager.HttpGetTimeslots(tokenValue);

                // Updated each RelayGroup Setting Toggles (Manual vs Committee vs Bookings)
                foreach (var relayGroup in latestSettings.RelayGroupsSettings) // 4 Loops
                {

                    // Handle RelayGroup Settings
                    var mode = relayGroup.Value.Mode;
                    if (mode == GroupSettingMode.gsmManual)
                    {
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsRelayGroupModeAutomate_Value = false;                        
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsAutomateTypeBooking_Disabled = true;
                        curDashboardVM.RelayGroupList[relayGroup.Key].ManualMode_BoxCssClass = $"boxActive";
                        curDashboardVM.RelayGroupList[relayGroup.Key].AutomateMode_BoxCssClass = $"boxInActive";
                        curDashboardVM.RelayGroupList[relayGroup.Key].ManualSwitchTogglesDisabled = false;
                      //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Relay Group {relayGroup.Key} - Mode Value {mode} - Set as Manual");
                    }
                    else if (mode == GroupSettingMode.gsmInternal)
                    {
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsRelayGroupModeAutomate_Value = true;                        
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsAutomateTypeBooking_Value = false;
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsAutomateTypeBooking_Disabled = false;
                        curDashboardVM.RelayGroupList[relayGroup.Key].ManualMode_BoxCssClass = $"boxInActive";
                        curDashboardVM.RelayGroupList[relayGroup.Key].AutomateMode_BoxCssClass = $"boxActive";
                        curDashboardVM.RelayGroupList[relayGroup.Key].ManualSwitchTogglesDisabled = true;
                        curDashboardVM.RelayGroupList[relayGroup.Key].AutomateMode_Text = "Committee Mode";
                        //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Relay Group {relayGroup.Key} - Mode Value {mode} - Set as Committee");
                    }
                    else if (mode == GroupSettingMode.gsmExternal)
                    {
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsRelayGroupModeAutomate_Value = true;                        
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsAutomateTypeBooking_Value = true;
                        curDashboardVM.RelayGroupList[relayGroup.Key].IsAutomateTypeBooking_Disabled = false;
                        curDashboardVM.RelayGroupList[relayGroup.Key].ManualMode_BoxCssClass = $"boxInActive";
                        curDashboardVM.RelayGroupList[relayGroup.Key].AutomateMode_BoxCssClass = $"boxActive";
                        curDashboardVM.RelayGroupList[relayGroup.Key].ManualSwitchTogglesDisabled = true;
                        curDashboardVM.RelayGroupList[relayGroup.Key].AutomateMode_Text = "Bookings Mode";
                        //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Relay Group {relayGroup.Key} - Mode Value {mode} - Set as Bookings");
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Error, $"[Dashboard] Relay Group {relayGroup.Key} - Mode Value {mode} - Set as UNKNOWN");
                    }
                }

                // Updated each RelayGroup Relay Toggles (A vs B vs C) + Timeslot Data for next Light Event
                foreach (var relayGroupList in curDashboardVM.RelayGroupList) // 4 Loops
                {
                    // Updated each RelayGroup Relay Toggles (A vs B vs C)
                    var relayListByGroup = (from rG in latestRelays
                                            where rG.RelayGroupId == relayGroupList.Key
                                            select rG).ToList();
                    if(relayListByGroup != null)
                    {                        
                        foreach (var item in relayListByGroup)
                        {
                            var Mod3OfId = (item.Id % 3);
                            if(Mod3OfId == 1)
                            {
                                bool stateToSet = ((int)item.State == 1) ? true : false;
                                curDashboardVM.RelayGroupList[relayGroupList.Key].RelayA_Value = stateToSet;
                                curDashboardVM.RelayGroupList[relayGroupList.Key].ManualModeA_Value = stateToSet;
                              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Relay Group {relayGroupList.Key} - Relay Node {item.Id} - Update Relay {relayGroupList.Key}1 (A) with {stateToSet}");
                            }
                            else if (Mod3OfId == 2)
                            {
                                bool stateToSet = ((int)item.State == 1) ? true : false;
                                curDashboardVM.RelayGroupList[relayGroupList.Key].RelayB_Value = stateToSet;
                                curDashboardVM.RelayGroupList[relayGroupList.Key].ManualModeB_Value = stateToSet;
                              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Relay Group {relayGroupList.Key} - Relay Node {item.Id} - Update Relay {relayGroupList.Key}2 (B) with {stateToSet}");
                            }
                            else if (Mod3OfId == 0)
                            {
                                bool stateToSet = ((int)item.State == 1) ? true : false;
                                curDashboardVM.RelayGroupList[relayGroupList.Key].RelayC_Value = stateToSet;
                                curDashboardVM.RelayGroupList[relayGroupList.Key].ManualModeC_Value = stateToSet;
                              //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Relay Group {relayGroupList.Key} - Relay Node {item.Id} - Update Relay {relayGroupList.Key}3 (C) with {stateToSet}");
                            }
                            else
                            {
                                Loggy.Log(Loggy.LogType.Error, $"[Dashboard] Relay Group {relayGroupList.Key} - Relay Node {item.Id} - Relay Toggle not Updated");
                            }
                        }                       
                    }

                    // Updated Timeslot Data for next Light Event
                    var nextTimeslotWithLightsOn = (from ts in latestTimeslots
                                                    where ts.State == TimeSlotDataModel.TimeSlotState.On
                                                    && ts.RelayGroupId == relayGroupList.Key
                                                    && ts.TimeFrom.Value.ToLocalTime() >= DateTime.Now.ToLocalTime()
                                                    orderby ts.OriginalTimeFrom.Value.ToLocalTime()
                                                    select ts).FirstOrDefault();

                    if(nextTimeslotWithLightsOn != null)
                    {
                        //Loggy.Log(Loggy.LogType.Debug, $"[Dashboard] Timeslot Id - {nextTimeslotWithLightsOn.Id} - TimeFrom {nextTimeslotWithLightsOn.TimeFrom} - TimeTo {nextTimeslotWithLightsOn.TimeTo} - OrigTimeFrom {nextTimeslotWithLightsOn.OriginalTimeFrom} - OrigTimeTo {nextTimeslotWithLightsOn.OriginalTimeTo} - State {nextTimeslotWithLightsOn.State} - Type {nextTimeslotWithLightsOn.Type}");
                        //Loggy.Log(Loggy.LogType.Info, "XTEST-BaseUtcOffset - " + TimeZoneInfo.Local.BaseUtcOffset);
                        //Loggy.Log(Loggy.LogType.Info, "XTEST-NowTime - " + DateTime.Now.ToString("HH:mm"));
                        //Loggy.Log(Loggy.LogType.Info, "XTEST-NowUniversalTime - " + DateTime.Now.ToUniversalTime().ToString("HH:mm"));
                        //Loggy.Log(Loggy.LogType.Info, "XTEST-NowLocalTime - " + DateTime.Now.ToLocalTime().ToString("HH:mm"));

                        //Loggy.Log(Loggy.LogType.Info, "XDATA-FromNowTime - " + nextTimeslotWithLightsOn.TimeFrom.Value.ToString("HH:mm"));
                        //Loggy.Log(Loggy.LogType.Info, "XDATA-FromNowUniversalTime - " + nextTimeslotWithLightsOn.TimeFrom.Value.ToUniversalTime().ToString("HH:mm"));
                        //Loggy.Log(Loggy.LogType.Info, "XDATA-FromNowLocalTime - " + nextTimeslotWithLightsOn.TimeFrom.Value.ToLocalTime().ToString("HH:mm"));

                        //Loggy.Log(Loggy.LogType.Info, "XDATA-ToNowTime - " + nextTimeslotWithLightsOn.TimeTo.Value.ToString("HH:mm"));
                        //Loggy.Log(Loggy.LogType.Info, "XDATA-ToNowUniversalTime - " + nextTimeslotWithLightsOn.TimeTo.Value.ToUniversalTime().ToString("HH:mm"));
                        //Loggy.Log(Loggy.LogType.Info, "XDATA-ToNowLocalTime - " + nextTimeslotWithLightsOn.TimeTo.Value.ToLocalTime().ToString("HH:mm"));

                        var todayFromTimeToTime = $"{nextTimeslotWithLightsOn.TimeFrom.Value.ToLocalTime().ToString("ddd, dd MMM HH:mm")} - {nextTimeslotWithLightsOn.OriginalTimeTo.Value.ToLocalTime().ToString("HH:mm")}";
                        curDashboardVM.RelayGroupList[relayGroupList.Key].AutomateMode_String02 = todayFromTimeToTime;
                        var byJohnDoe = $"By: {nextTimeslotWithLightsOn.CreatedByUsername}";
                        curDashboardVM.RelayGroupList[relayGroupList.Key].AutomateMode_String03 = byJohnDoe;
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Warning, $"[Dashboard] Next Lit Timeslot = NULL");
                    }
                }

                return curDashboardVM;

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        private async Task HandleSettingModeChange(string relayGroupKey, bool valueChangedTo, bool IsCourtChange = false, bool IsAutomateChange = false)
        {            
            try
            {
                Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleSettingModeChange - relayGroupKey: {relayGroupKey} - valueChangedTo: {valueChangedTo} - IsCourtChange: {IsCourtChange} - IsAutomateChange: {IsAutomateChange}");
                
                var rgCurrentCourtMode = _DashboardVM.RelayGroupList[relayGroupKey].IsRelayGroupModeAutomate_Value;
                var rgCurrentAutomateMode = _DashboardVM.RelayGroupList[relayGroupKey].IsAutomateTypeBooking_Value;

                var newRelayGroupMode = String.Empty;
                if (IsCourtChange && !IsAutomateChange)         // Top Toggle (Manual VS Auto) Flag Changed
                {
                    if(valueChangedTo == false) 
                    {
                        newRelayGroupMode = GroupSettingMode.gsmManual;
                    }
                    else 
                    {
                        if(rgCurrentAutomateMode == false)
                        {
                            newRelayGroupMode = GroupSettingMode.gsmInternal;
                        }
                        else
                        {
                            newRelayGroupMode = GroupSettingMode.gsmExternal;
                        }                        
                    }
                }
                else if(!IsCourtChange && IsAutomateChange && rgCurrentCourtMode == true)     // Bottom Toggle (Commitee VS Booking) Flag Changed
                {
                    if (valueChangedTo == false)
                    {
                        newRelayGroupMode = GroupSettingMode.gsmInternal;
                    }
                    else
                    {
                        newRelayGroupMode = GroupSettingMode.gsmExternal;
                    }
                }
                else
                {
                    Loggy.Log(Loggy.LogType.Error, $" HandleSettingModeChange - Never should get this message since both false");
                }

                Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleSettingModeChange - CurCourtMode = {rgCurrentCourtMode} / CurAutoMode = {rgCurrentAutomateMode} - NewRelayGroupMode = {newRelayGroupMode}");

                var tokenValue = await GetAccessTokenValue();
                var updateSettingsGroupModeResult = await _AppManager.HttpUpdateSettingsGroupMode(tokenValue, relayGroupKey, newRelayGroupMode);

                if (updateSettingsGroupModeResult != null)
                {
                    if(updateSettingsGroupModeResult.RelayGroupsSettings[relayGroupKey].Mode == newRelayGroupMode)
                    {
                        Loggy.Log(Loggy.LogType.Info, $"Successfully Updated: Relay Group Key <{relayGroupKey}> Mode to <{newRelayGroupMode}>");
                    }
                    else
                    {
                        Loggy.Log(Loggy.LogType.Info, $"Failed to Update: Relay Group Key <{relayGroupKey}> Mode to <{newRelayGroupMode}>");
                    }                   
                }
                else
                {
                    Loggy.Log(Loggy.LogType.Info, $"Failed to Update: Relay Group Key <{relayGroupKey}> Mode to <{newRelayGroupMode}>");                    
                }

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
            finally
            {
                _DashboardVM = await GetUpdatedDashboardData();
            }
        }

        private async Task HandleManualSwitchChange(string relayKey, bool valueChangedTo)
        {
            try
            {                
              //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleManualSwitchChange - relayKey: {relayKey} - valueChangedTo: {valueChangedTo}");

                var tokenValue = await GetAccessTokenValue();
                int relayId = -1;
                int newRelayState = (valueChangedTo) ? 1 : 0;              

                switch (relayKey)
                {
                    case "11": relayId = 1; break;
                    case "12": relayId = 2; break;
                    case "13": relayId = 3; break;
                    case "21": relayId = 4; break;
                    case "22": relayId = 5; break;
                    case "23": relayId = 6; break;
                    case "31": relayId = 7; break;
                    case "32": relayId = 8; break;
                    case "33": relayId = 9; break;
                    case "41": relayId = 10; break;
                    case "42": relayId = 11; break;
                    case "43": relayId = 12; break;
                    default: break;
                }

                if(relayId != -1)
                {
                    //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleManualSwitchChange - relayId: {relayId} - newRelayState: {newRelayState}");
                    var updateRelayResult = await _AppManager.HttpUpdateRelay(tokenValue, relayId, newRelayState);
                    //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleManualSwitchChange - updateRelayResult: {updateRelayResult}");
                    if (updateRelayResult != null)
                    {
                      
                        var resultState = (int)(updateRelayResult.Find(x => x.Id == relayId).State);
                        //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleManualSwitchChange - resultState: {resultState} VS newRelayState: {newRelayState}");
                        if (resultState == newRelayState)
                        {
                            Loggy.Log(Loggy.LogType.Info, $"Successfully Updated: Relay Key {relayKey} to {valueChangedTo} -  relayId: {relayId} - newRelayState: {newRelayState}");
                        }
                        else
                        {                                                      
                            Loggy.Log(Loggy.LogType.Info, $"Failed to Update: Relay Key {relayKey} to {valueChangedTo} -  relayId: {relayId} - newRelayState: {newRelayState}");
                        }
                    }
                    else
                    {                                                
                        Loggy.Log(Loggy.LogType.Info, $"Failed to Update: Relay Key {relayKey} to {valueChangedTo} -  relayId: {relayId} - newRelayState: {newRelayState}");
                      //Loggy.Log(Loggy.LogType.Debug, $"IN Dashboard().HandleManualSwitchChange :: updateRelayResult = null  - relayId: {relayId} - newRelayState: {newRelayState}");
                    }
                }                
            }
            catch (Exception ex)
            {                
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
            finally
            {
                _DashboardVM = await GetUpdatedDashboardData();
            }
            
        }


        // HELPER METHODS
        public async Task<string> GetAccessTokenValue()
        {
            //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token String");
            try
            {
                var token = await GetAccessToken();
                string tokenValue = token.Value;
                _AppManager.SetAppAccessTokenValue(tokenValue);
                return tokenValue;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public async Task<AccessToken> GetAccessToken()
        {
            //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token");
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, "[TestPage] Call Get TokenProvider Request Access Token");
                var accessTokenResult = await _TokenProvider.RequestAccessToken();
                //Loggy.Log(Loggy.LogType.Debug, "accessTokenResult = " + accessTokenResult.ToString());
                if (accessTokenResult.TryGetToken(out var token))
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Success : Token = " + token.ToString());
                    return token;
                }
                else
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Fail : Retuning Null ");
                    _NavigationManager.NavigateTo("/authentication/login");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

    }
}
