﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Syncfusion.Blazor.Schedule;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;
using VortexLoop.BlazorApp.ModelsView;

namespace VortexLoop.BlazorApp.Pages
{
    public partial class Scheduler
    {
        [CascadingParameter] Task<AuthenticationState> _AuthenticationState { get; set; }
        [Inject] NavigationManager _NavigationManager { get; set; }
        [Inject] HttpClient _HttpClient { get; set; }
        [Inject] IAccessTokenProvider _TokenProvider { get; set; }
        [Inject] SignOutSessionStateManager _SignOutManager { get; set; }
        [Inject] AppManager _AppManager { get; set; }

        public SchedulerVM _SchedulerVM { get; set; }

        // Component References        
        //--private Dictionary<string, SfSwitch<bool>> ManualSwitchToggles = new Dictionary<string, SfSwitch<bool>>();

        // Blazor LifeCycle Trigger 00
        public Scheduler()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Scheduler] Scheduler() = BLT00");
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, $"IN Scheduler() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        
        // Blazor LifeCycle Trigger 02
        protected override void OnInitialized()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Scheduler] OnInitialized() - BLT02");
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, $"[Scheduler].OnInitialized() - AppManager = {_AppManager}");
                //Loggy.Log(Loggy.LogType.Debug, $"[Scheduler].OnInitialized() - HttpClient = {_HttpClient}");
                //Loggy.Log(Loggy.LogType.Debug, $"[Scheduler].OnInitialized() - TokenProvider = {_TokenProvider}");
                //Loggy.Log(Loggy.LogType.Debug, $"[Scheduler].OnInitialized() - NavigationManager = {_NavigationManager}");

                _SchedulerVM = GetInitialSchedulerVM();
                
                //OnInitialisedPoC();

                //Loggy.Log(Loggy.LogType.Debug, $"IN [Scheduler].OnInitialized() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 03
        protected override async Task OnInitializedAsync()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Scheduler] OnInitializedAsync() - BLT03");
            try
            {
                _SchedulerVM = await GetUpdatedSchedulerData();

                //var schedulerVMJsonString = JsonSerializer.Serialize<SchedulerVM>(_SchedulerVM, new JsonSerializerOptions() { WriteIndented = true });
                //Loggy.Log(Loggy.LogType.Debug, schedulerVMJsonString);


                //await InvokeAsync(() =>
                //{
                //    StateHasChanged();
                //});

                //Loggy.Log(Loggy.LogType.Debug, $"IN [Scheduler].OnInitializedAsync() - {ManualSwitchToggles["22"].Checked} - {_DashboardVM.RelayGroupList["2"].ManualModeB_Value}");
                //Loggy.Log(Loggy.LogType.Debug, $"IN [Scheduler].OnInitializedAsync() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 04 & 06
        //protected override async Task OnAfterRenderAsync(bool firstRender)
        //{
        //    //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Scheduler] OnAfterRenderAsync(bool firstRender) - BLT04_BLT06");
        //    try
        //    {
        //        //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard] firstRender = {firstRender.ToString()}");                
        //        if (firstRender)
        //        {
        //            Loggy.Log(Loggy.LogType.Debug, $"IN [Scheduler].OnAfterRenderAsync(bool firstRender) - FIRST RENDER - BLT04");
        //        }
        //        else
        //        {
        //            Loggy.Log(Loggy.LogType.Debug, $"IN [Scheduler].OnAfterRenderAsync(bool firstRender) - NEXT RENDER - BLT06");
        //        }
        //        //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnInitializedAsync() - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}


        // Blazor LifeCycle Trigger 05
        protected override void OnParametersSet()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] OnParametersSet() - BLT05"); 
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnParametersSet() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // -----------------------------------------------------------
        // Frontend Events

        // -- TODO

        // -----------------------------------------------------------
        // Dashboard Methods

        private SchedulerVM GetInitialSchedulerVM()
        {
            Loggy.Log(Loggy.LogType.Debug, "[Scheduler] Entered GetInitialSchedulerVM()");
            try
            {
                SchedulerVM schedulerVM = new SchedulerVM();
                schedulerVM.ScheduleDictionary = new Dictionary<string, ScheduleVM>();

                schedulerVM.ComScheduleDictionary = new Dictionary<string, ComScheduleVM>();
                schedulerVM.ComScheduleList = new List<ComScheduleVM>();

                schedulerVM.ComScheduleRelayGroupDictionary = new Dictionary<string, ComScheduleRelayGroupVM>();
                schedulerVM.ComScheduleRelayGroupList = new List<ComScheduleRelayGroupVM>();

                schedulerVM.ComScheduleTypeDictionary = new Dictionary<string, ComScheduleTypeVM>();
                schedulerVM.ComScheduleTypeList = new List<ComScheduleTypeVM>();

                return schedulerVM;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        private async Task<SchedulerVM> GetUpdatedSchedulerData()
        {
            Loggy.Log(Loggy.LogType.Debug, "[Scheduler] Entered GetUpdatedSchedulerData()");
            try
            {
                SchedulerVM curSchedulerVM = new SchedulerVM();

                var tokenValue = await GetAccessTokenValue();

                var latestTimeslots = new List<TimeSlotDataModel>();
                latestTimeslots = await _AppManager.HttpGetTimeslots(tokenValue);

                var fromDT = DateTime.Now;
                var toDT = DateTime.Now.AddDays(15);

                var lessTimeslots = (from x in latestTimeslots
                                     where x.OriginalTimeFrom >= new DateTime(fromDT.Year, fromDT.Month, fromDT.Day, 0, 0, 0)
                                     && x.OriginalTimeTo <= new DateTime(toDT.Year, toDT.Month, toDT.Day, 23, 59, 59)
                                     orderby x.OriginalTimeFrom
                                     select x).ToList();

                curSchedulerVM.ScheduleDictionary = new Dictionary<string, ScheduleVM>();                
                curSchedulerVM.ComScheduleDictionary = new Dictionary<string, ComScheduleVM>();
                curSchedulerVM.ComScheduleList = new List<ComScheduleVM>();
                curSchedulerVM.ComScheduleRelayGroupDictionary = new Dictionary<string, ComScheduleRelayGroupVM>();
                curSchedulerVM.ComScheduleRelayGroupList = new List<ComScheduleRelayGroupVM>();
                curSchedulerVM.ComScheduleTypeDictionary = new Dictionary<string, ComScheduleTypeVM>();
                curSchedulerVM.ComScheduleTypeList = new List<ComScheduleTypeVM>();

                foreach (var tS in lessTimeslots)
                {
                    ScheduleVM scheduleVM = new ScheduleVM();
                    if (!curSchedulerVM.ScheduleDictionary.ContainsKey(tS.Id))
                    {
                        curSchedulerVM.ScheduleDictionary.Add(tS.Id, scheduleVM);
                    }    

                    curSchedulerVM.ScheduleDictionary[tS.Id].TimeSlot = new TimeSlotDataModel();
                    curSchedulerVM.ScheduleDictionary[tS.Id].ComSchedule = new ComScheduleVM();
                    curSchedulerVM.ScheduleDictionary[tS.Id].ComScheduleRelayGroup = new ComScheduleRelayGroupVM();
                    curSchedulerVM.ScheduleDictionary[tS.Id].ComScheduleType = new ComScheduleTypeVM();

                    ComScheduleVM comScheduleVM = new ComScheduleVM();
                    ComScheduleRelayGroupVM comScheduleRelayGroupVM = new ComScheduleRelayGroupVM();
                    ComScheduleTypeVM comScheduleTypeVM = new ComScheduleTypeVM();

                    // -- TimeSlotDataModel
                    // TimeSlot
                    curSchedulerVM.ScheduleDictionary[tS.Id].TimeSlot = tS;

                    // -- ComScheduleVM
                    // Id
                    comScheduleVM.Id = tS.Id;
                    // Subject
                    comScheduleVM.Subject = tS.CreatedByUsername;
                    // StartTime
                    comScheduleVM.StartTime = tS.OriginalTimeFrom.Value.ToLocalTime();
                    // EndTime
                    comScheduleVM.EndTime = tS.OriginalTimeTo.Value.ToLocalTime();
                    // State
                    var isStateOn = (tS.State == TimeSlotDataModel.TimeSlotState.On) ? true : false;
                    comScheduleVM.State = isStateOn;
                    // IsReadOnly
                    var isReadOnly = (tS.Type == TimeSlotMode.tstExternal) ? true : false;
                    comScheduleVM.IsReadonly = isReadOnly;
                    // RefRelayGroupKey - ForeignKey
                    comScheduleVM.RefRelayGroupKey = tS.RelayGroupId;                    
                    // RefTypeKey - ForeignKey
                    comScheduleVM.RefTypeKey = tS.Type;
                    // + ScheduleVM
                    curSchedulerVM.ScheduleDictionary[tS.Id].ComSchedule = comScheduleVM;

                    // -- ComScheduleRelayGroupVM
                    // Key
                    comScheduleRelayGroupVM.Key = tS.RelayGroupId;
                    // RelayGroupText // RelayGroupColor
                    var rgText = String.Empty;
                    var rgColor = String.Empty;
                    if(tS.RelayGroupId == RelayGroupKeys.rg1)
                    {
                        rgText = "Court 01";
                        rgColor = "#444";
                    }
                    if (tS.RelayGroupId == RelayGroupKeys.rg2)
                    {
                        rgText = "Court 02";
                        rgColor = "#555";
                    }
                    if (tS.RelayGroupId == RelayGroupKeys.rg3)
                    {
                        rgText = "Court 03";
                        rgColor = "#666";
                    }
                    if (tS.RelayGroupId == RelayGroupKeys.rg4)
                    {
                        rgText = "Court 04";
                        rgColor = "#777";
                    }
                    comScheduleRelayGroupVM.RelayGroupText = rgText;
                    comScheduleRelayGroupVM.RelayGroupColor = rgColor;
                    // + ScheduleVM
                    curSchedulerVM.ScheduleDictionary[tS.Id].ComScheduleRelayGroup = comScheduleRelayGroupVM;

                    // -- ComScheduleTypeVM
                    // Key
                    comScheduleTypeVM.Key = tS.Type;
                    // TypeText // TypeColor
                    var typeText = String.Empty;
                    var typeColor = String.Empty;
                    if (tS.Type == GroupSettingMode.gsmExternal)
                    {
                        typeText = "Bookings";
                        typeColor = "#888";
                    }
                    if (tS.Type == GroupSettingMode.gsmInternal)
                    {
                        typeText = "Committee";
                        typeColor = "#999";
                    }
                    comScheduleTypeVM.TypeText = typeText;
                    comScheduleTypeVM.TypeColor = typeColor;
                    // + ScheduleVM
                    curSchedulerVM.ScheduleDictionary[tS.Id].ComScheduleType = comScheduleTypeVM;

                    // Add to Dictionary
                    curSchedulerVM.ComScheduleDictionary.Add(comScheduleVM.Id, comScheduleVM);
                    curSchedulerVM.ComScheduleList.Add(comScheduleVM);
                    if (!curSchedulerVM.ComScheduleRelayGroupDictionary.ContainsKey(comScheduleRelayGroupVM.Key))
                    {
                        curSchedulerVM.ComScheduleRelayGroupDictionary.Add(comScheduleRelayGroupVM.Key, comScheduleRelayGroupVM);
                        curSchedulerVM.ComScheduleRelayGroupList.Add(comScheduleRelayGroupVM);
                        curSchedulerVM.ComScheduleRelayGroupList = curSchedulerVM.ComScheduleRelayGroupList.OrderBy(o => o.Key).ToList();
                    }
                    if (!curSchedulerVM.ComScheduleTypeDictionary.ContainsKey(comScheduleTypeVM.Key))
                    {
                        curSchedulerVM.ComScheduleTypeDictionary.Add(comScheduleTypeVM.Key, comScheduleTypeVM);
                        curSchedulerVM.ComScheduleTypeList.Add(comScheduleTypeVM);
                        curSchedulerVM.ComScheduleTypeList = curSchedulerVM.ComScheduleTypeList.OrderBy(o => o.Key).ToList();
                    }
                }

                return curSchedulerVM;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // HELPER METHODS
        public async Task<string> GetAccessTokenValue()
        {
            //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token String");
            try
            {
                var token = await GetAccessToken();
                string tokenValue = token.Value;
                _AppManager.SetAppAccessTokenValue(tokenValue);
                return tokenValue;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public async Task<AccessToken> GetAccessToken()
        {
            //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token");
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, "[TestPage] Call Get TokenProvider Request Access Token");
                var accessTokenResult = await _TokenProvider.RequestAccessToken();
                //Loggy.Log(Loggy.LogType.Debug, "accessTokenResult = " + accessTokenResult.ToString());
                if (accessTokenResult.TryGetToken(out var token))
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Success : Token = " + token.ToString());
                    return token;
                }
                else
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Fail : Retuning Null ");
                    _NavigationManager.NavigateTo("/authentication/login");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }




        [Parameter]
        public SfSchedule<ComScheduleVM> sfScheduleX { get; set; }
        [Parameter]
        public List<ComScheduleVM> scheduleViewModel { get; set; }
        [Parameter]
        public List<ComScheduleRelayGroupVM> rgViewModel { get; set; }
        [Parameter]
        public List<ComScheduleTypeVM> typeViewModel { get; set; }

        public string[] Resources { get; set; } = { "Courts", "Types" };
        public DateTime CurrentDate = DateTime.Now;        

    }


}
