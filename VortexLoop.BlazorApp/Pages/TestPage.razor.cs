﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Syncfusion.Blazor.Grids;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Pages
{
    public partial class TestPage
    {
        [CascadingParameter] public Task<AuthenticationState> AuthenticationState { get; set; }
        [Inject] public NavigationManager _NavigationManager { get; set; }
        [Inject] public HttpClient HttpClient { get; set; }
        [Inject] public IAccessTokenProvider TokenProvider { get; set; }
        [Inject] public SignOutSessionStateManager SignOutManager { get; set; }
        [Inject] public AppManager AppManager { get; set; }

        // RELAYS
        [Parameter]
        public SfGrid<RelayDataModel> gridRelaysRef { get; set; }
        private List<RelayDataModel> TestRelayList { get; set; }
        private int RelayInputId { get; set; }
        private int RelayInputState { get; set; }

        // TIMESLOTS
        [Parameter]
        public SfGrid<TimeSlotDataModel> gridTimeslotsRef { get; set; }
        private List<TimeSlotDataModel> TestTimeSlotList { get; set; }
        private string TimeslotInputId { get; set; }
        private int TimeslotInputState { get; set; }

        // SETTINGS
        //[Parameter]
        //public SfGrid<TimeSlotDataModel> gridTimeslotsRef { get; set; }
        private SettingDataModel TestSettings { get; set; }
        //private string TimeslotInputId { get; set; }
        //private int TimeslotInputState { get; set; }

        // CONSTRUCTOR
        public TestPage()
        {
        }

        protected override async Task OnInitializedAsync()
        {
            Loggy.Log(Loggy.LogType.Info, $"START CODE for TestPage");
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"[TestPageInitializedAsync] HttpClient = {HttpClient}");
              //Loggy.Log(Loggy.LogType.Debug, $"[TestPageInitializedAsync] TokenProvider = {TokenProvider}");
              //Loggy.Log(Loggy.LogType.Debug, $"[TestPageInitializedAsync] NavigationManager = {_NavigationManager}");

                Loggy.Log(Loggy.LogType.Info, $"Try Get Access Token on TestPage");

                var tokenValue = await GetAccessTokenValue();                
                Loggy.Log(Loggy.LogType.Info, $"Check Token Value = " + tokenValue);

                TestRelayList = new List<RelayDataModel>();
                TestRelayList = await AppManager.HttpGetRelays(tokenValue);

                TestTimeSlotList = new List<TimeSlotDataModel>();
                TestTimeSlotList = await AppManager.HttpGetTimeslots(tokenValue);

                TestSettings = new SettingDataModel();
                TestSettings = await AppManager.HttpGetSettings(tokenValue);

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        public void BtnRefreshData(MouseEventArgs e)
        {
            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY REFRESH CLICKED");

            _NavigationManager.NavigateTo(_NavigationManager.Uri, forceLoad: true);
        }
        public async Task BtnRelayUpdate(MouseEventArgs e)
        {
            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY UPDATED CLICKED");

            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY UPDATED CLICKED - ID Value = {RelayInputId}");
            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY UPDATED CLICKED - State Value = {RelayInputState}");

            var tokenValue = await GetAccessTokenValue();
            var relayDataModelList = await AppManager.HttpUpdateRelay(tokenValue, RelayInputId, RelayInputState);
          //Loggy.Log(Loggy.LogType.Debug, $"Logging X Result = {relayDataModelList}");

            TestRelayList = relayDataModelList;           
        }
        public async Task BtnTimeslotUpdate(MouseEventArgs e)
        {
            Loggy.Log(Loggy.LogType.Info, $"BTN TIMESLOT UPDATED CLICKED");

            Loggy.Log(Loggy.LogType.Info, $"BTN TIMESLOT UPDATED CLICKED - ID Value = {TimeslotInputId}");
            Loggy.Log(Loggy.LogType.Info, $"BTN TIMESLOT UPDATED CLICKED - State Value = {TimeslotInputState}");

            var tokenValue = await GetAccessTokenValue();
            var timeslotDataModelList = await AppManager.HttpUpdateTimeslot(tokenValue, TimeslotInputId, TimeslotInputState);
          //Loggy.Log(Loggy.LogType.Debug, $"Logging X Result = {timeslotDataModelList}");

            TestTimeSlotList = timeslotDataModelList;
        }

        // HELPER METHODS
        public async Task<string> GetAccessTokenValue()
        {
          //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token String");
            try
            {
                var token = await GetAccessToken();
                string tokenValue = token.Value;
                AppManager.SetAppAccessTokenValue(tokenValue);
                return tokenValue;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public async Task<AccessToken> GetAccessToken()
        {
          //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token");
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, "[TestPage] Call Get TokenProvider Request Access Token");
                var accessTokenResult = await TokenProvider.RequestAccessToken();
              //Loggy.Log(Loggy.LogType.Debug, "accessTokenResult = " + accessTokenResult.ToString());
                if (accessTokenResult.TryGetToken(out var token))
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Success : Token = " + token.ToString());
                    return token;
                }
                else
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Fail : Retuning Null ");
                    _NavigationManager.NavigateTo("/authentication/login");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

    }
}
