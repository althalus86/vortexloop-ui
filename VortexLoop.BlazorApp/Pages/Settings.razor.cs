﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using Syncfusion.Blazor.Grids;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp.Pages
{
    public class MarkerData
    {
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
    public partial class Settings
    {
        [CascadingParameter] public Task<AuthenticationState> AuthenticationState { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }
        [Inject] public HttpClient HttpClient { get; set; }
        [Inject] public IAccessTokenProvider TokenProvider { get; set; }
        [Inject] public SignOutSessionStateManager SignOutManager { get; set; }
        [Inject] public AppManager AppDataManager { get; set; }

        // SECURITY
        private string AccessTokenValue { get; set; }

        // RELAYS
        [Parameter]
        public SfGrid<RelayDataModel> gridRelaysRef { get; set; }
        private List<RelayDataModel> TestRelayList { get; set; }
        private int RelayInputId { get; set; }
        private int RelayInputState { get; set; }

        private JsonDocument SettingsDocument { get; set; }
        private SettingDataModel SettingsData { get; set; }
        private List<MarkerData> MarkerDatas { get; set; }

        // CONSTRUCTOR
        protected override async Task OnInitializedAsync()
        {
            try
            {
                var accessToken = await GetAccessToken();
                AccessTokenValue = accessToken.Value;

                SettingsData = await GetSettings();
                MarkerDatas = new List<MarkerData> {
                    new MarkerData { Name="Device Location",  Latitude=SettingsData.Latitude, Longitude=SettingsData.Longitude }
                };
                // SettingsDocument = JsonDocument.Parse(SettingsString);
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                // User might be loggedout
                throw;
            }
        }


        public async Task<SettingDataModel> GetSettings(bool ReFetch = true)
        {
            var settings = new SettingDataModel();
            HttpResponseMessage httpResponseMessage = await SecureApiCall(HttpMethod.Get, "/api/settings");
            if (httpResponseMessage != null)
            {
                if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                {
                    var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                    if (responseBody != null)
                    {
                        settings = JsonSerializer.Deserialize<SettingDataModel>(responseBody);
                        Console.WriteLine(settings);
                        Loggy.Log(Loggy.LogType.Info, $"settings= {settings}");

                        /* if (freshRelayList != null && freshRelayList.Count > 0)
                         {
                             latestRelayList = AppDataManager.UpdateRelayList(freshRelayList);
                         }*/
                    }
                }
                else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                {
                    throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase);
                }
            }
            else
            {
                throw new Exception("HttpResponse=Null - Something wend wrong");
            }
            return settings;
        }



        public void BtnRefreshData(MouseEventArgs e)
        {
            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY REFRESH CLICKED");

            NavigationManager.NavigateTo(NavigationManager.Uri, forceLoad: true);
        }
        public async Task BtnRelayUpdate(MouseEventArgs e)
        {
            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY UPDATED CLICKED");

            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY UPDATED CLICKED - ID Value = {RelayInputId}");
            Loggy.Log(Loggy.LogType.Info, $"BTN RELAY UPDATED CLICKED - State Value = {RelayInputState}");

            var x = await UpdateRelay(RelayInputId, RelayInputState);
          //Loggy.Log(Loggy.LogType.Debug, $"Logging X Result = {x}");

            TestRelayList = x;


            //BtnRefreshData(new MouseEventArgs());
        }


        public async Task<List<RelayDataModel>> UpdateRelay(int id, int state)
        {
            var updatedRelayList = await UpdateRelay(id, (RelayDataModel.RelayState)state);
            return updatedRelayList;
        }

        public async Task<List<RelayDataModel>> UpdateRelay(int id, RelayDataModel.RelayState state)
        {
            try
            {
                var latestRelayList = AppDataManager.GetRelayList();

              //Loggy.Log(Loggy.LogType.Debug, $"Creating new Relay Model to Update Relay with ID = {id} and State = {state}");


                RelayDataModel newRelayDataModel = AppDataManager.GetRelayList().Find(item => item.Id == id);
                newRelayDataModel.State = state;
                string jsonBody = JsonSerializer.Serialize(newRelayDataModel);
              //Loggy.Log(Loggy.LogType.Debug, $"New Json Body String = {jsonBody}");

                HttpResponseMessage httpResponseMessage = await SecureApiCall(HttpMethod.Put, AppDataManager.GetAppSettings().RELAY_RESOURCE_ROUTE, jsonBody, newRelayDataModel.Id.ToString());
              //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                if (httpResponseMessage != null)
                {
                  //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                      //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                        var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                      //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                        if (responseBody != null)
                        {
                          //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                            List<RelayDataModel> freshRelayList = new List<RelayDataModel>();
                          //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                            freshRelayList = JsonSerializer.Deserialize<List<RelayDataModel>>(responseBody);
                          //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshRelayList);
                            if (freshRelayList != null && freshRelayList.Count > 0)
                            {
                              //Loggy.Log(Loggy.LogType.Debug, $"Fresh Relay List (Not Null) Count = " + freshRelayList.Count);
                                latestRelayList = AppDataManager.UpdateRelayList(freshRelayList);
                            }
                        }
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase);
                    }
                }
                return latestRelayList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // TIMESLOT METHODS
        public async Task<List<TimeSlotDataModel>> GetTimeslots(bool ReFetch = true)
        {
            try
            {
                var latestTimeslotList = AppDataManager.GetTimeslotList();
                if (ReFetch == false && latestTimeslotList != null)
                    return latestTimeslotList;
                else
                {
                    HttpResponseMessage httpResponseMessage = await SecureApiCall(HttpMethod.Get, AppDataManager.GetAppSettings().TIMESLOT_RESOURCE_ROUTE);
                  //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                    if (httpResponseMessage != null)
                    {
                      //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                        if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                        {
                          //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                            var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                          //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                            if (responseBody != null)
                            {
                              //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                                List<TimeSlotDataModel> freshTimeslotList = new List<TimeSlotDataModel>();
                              //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                                freshTimeslotList = JsonSerializer.Deserialize<List<TimeSlotDataModel>>(responseBody);
                              //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshTimeslotList);
                                if (freshTimeslotList != null && freshTimeslotList.Count > 0)
                                {
                                  //Loggy.Log(Loggy.LogType.Debug, $"Fresh Relay List (Not Null) Count = " + freshTimeslotList.Count);
                                    latestTimeslotList = AppDataManager.UpdateTimeslotList(freshTimeslotList);
                                }
                            }
                        }
                        else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                        {
                            throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase);
                        }
                    }
                    else
                    {
                        throw new Exception("HttpResponse=Null - Something wend wrong");
                    }

                    return latestTimeslotList;
                }

            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        public async Task<List<TimeSlotDataModel>> UpdateTimeslot(string id, int state)
        {
            var updatedTimeslotList = await UpdateTimeslot(id, (TimeSlotDataModel.TimeSlotState)state);
            return updatedTimeslotList;
        }
        public async Task<List<TimeSlotDataModel>> UpdateTimeslot(string id, TimeSlotDataModel.TimeSlotState state)
        {
            try
            {
                var latestTimeslotList = AppDataManager.GetTimeslotList();

              //Loggy.Log(Loggy.LogType.Debug, $"Creating new Timeslot Model to Update Timeslot with ID = {id} and State = {state}");

                TimeSlotDataModel newTimeslotDataModel = AppDataManager.GetTimeslotList().Find(item => item.Id == id);
                newTimeslotDataModel.State = state;
                string jsonBody = JsonSerializer.Serialize(newTimeslotDataModel);
              //Loggy.Log(Loggy.LogType.Debug, $"New Json Body String = {jsonBody}");

                HttpResponseMessage httpResponseMessage = await SecureApiCall(HttpMethod.Put, AppDataManager.GetAppSettings().TIMESLOT_RESOURCE_ROUTE, jsonBody, newTimeslotDataModel.Id);
              //Loggy.Log(Loggy.LogType.Debug, $"Got HTTP Response Back = " + httpResponseMessage);
                if (httpResponseMessage != null)
                {
                  //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response not NULL = " + httpResponseMessage.StatusCode);
                    if (httpResponseMessage.StatusCode == HttpStatusCode.OK)
                    {
                      //Loggy.Log(Loggy.LogType.Debug, $"HTTP Status OK - Reading Body Content next");
                        var responseBody = await httpResponseMessage.Content.ReadAsStringAsync();
                      //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body = " + responseBody);
                        if (responseBody != null)
                        {
                          //Loggy.Log(Loggy.LogType.Debug, $"HTTP Response Body Not Null = " + responseBody.Length.ToString());
                            List<TimeSlotDataModel> freshTimeslotList = new List<TimeSlotDataModel>();
                          //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Next");
                            freshTimeslotList = JsonSerializer.Deserialize<List<TimeSlotDataModel>>(responseBody);
                          //Loggy.Log(Loggy.LogType.Debug, $"Deserialize Complete - " + freshTimeslotList);
                            if (freshTimeslotList != null && freshTimeslotList.Count > 0)
                            {
                              //Loggy.Log(Loggy.LogType.Debug, $"Fresh Timeslot List (Not Null) Count = " + freshTimeslotList.Count);
                                latestTimeslotList = AppDataManager.UpdateTimeslotList(freshTimeslotList);
                            }
                        }
                    }
                    else if (httpResponseMessage.StatusCode == HttpStatusCode.Unauthorized)
                    {
                        throw new Exception("SecureApiCall Error: HttpCode=" + httpResponseMessage.StatusCode.ToString() + " | Message=" + httpResponseMessage.ReasonPhrase);
                    }
                }
                return latestTimeslotList;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // HELPER METHODS
        private async Task<HttpResponseMessage> SecureApiCall(HttpMethod httpMethod, string relativePath, string jsonStringContent = "", string resourceIndexString = "")
        {
            try
            {
                HttpResponseMessage httpResponseMessage = new HttpResponseMessage();
                HttpRequestMessage httpRequestMessage = new HttpRequestMessage();
                // Setting Http Method
                httpRequestMessage.Method = httpMethod;
                // Setting URI String
                string uriString = AppDataManager.GetAppSettings().ENVIRONMENT_API_BASE_URL + relativePath;
                uriString = (!String.IsNullOrEmpty(resourceIndexString)) ? uriString + "/" + resourceIndexString : uriString;
              //Loggy.Log(Loggy.LogType.Debug, "Computed Uri String = " + uriString);
                httpRequestMessage.RequestUri = new Uri(uriString);
                // Getting Access token
                var accessToken = await GetAccessToken();
                var accessTokenString = accessToken.Value;
                if (!String.IsNullOrEmpty(accessTokenString))
                {
                  //Loggy.Log(Loggy.LogType.Debug, "AccessToken Not Null or Empty");
                    httpRequestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessTokenString);
                }
                else
                {
                  //Loggy.Log(Loggy.LogType.Debug, "Access Token String Null or Empty");
                    httpResponseMessage.StatusCode = HttpStatusCode.Unauthorized;
                    httpResponseMessage.ReasonPhrase = "Authentication to Remote Server Unsuccessfull. Could not get an Access Token.";
                    return httpResponseMessage;
                }
                // Adding Body Content
              //Loggy.Log(Loggy.LogType.Debug, "Next Steps check if JsonString Content");
                if (jsonStringContent != String.Empty)
                {
                    httpRequestMessage.Content = new StringContent(jsonStringContent, Encoding.UTF8, "application/json");
                }
                // Preparing for HTTP Request
              //Loggy.Log(Loggy.LogType.Debug, "Pre HTTP Request");
                httpResponseMessage = await HttpClient.SendAsync(httpRequestMessage);
              //Loggy.Log(Loggy.LogType.Debug, "Post HTTP Request");
                // Outputing Response
                return httpResponseMessage;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        private async Task<AccessToken> GetAccessToken()
        {
          //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token");
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, "Call Get TokenProvider Request Access Token");
                var accessTokenResult = await TokenProvider.RequestAccessToken();
              //Loggy.Log(Loggy.LogType.Debug, "accessTokenResult = " + accessTokenResult.ToString());
                if (accessTokenResult.TryGetToken(out var token))
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Success : Token = " + token.ToString());
                    return token;
                }
                else
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Fail : Retuning Null ");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message}   | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

    }
}