﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;
using VortexLoop.BlazorApp.ModelsView;

namespace VortexLoop.BlazorApp.Pages
{
    public partial class Tabular
    {
        [CascadingParameter] Task<AuthenticationState> _AuthenticationState { get; set; }
        [Inject] NavigationManager _NavigationManager { get; set; }
        [Inject] HttpClient _HttpClient { get; set; }
        [Inject] IAccessTokenProvider _TokenProvider { get; set; }
        [Inject] SignOutSessionStateManager _SignOutManager { get; set; }
        [Inject] AppManager _AppManager { get; set; }

        public TabularVM _TabularExtract { get; set; }
        
        public DateTime DT_DatePicker { get; set; } = DateTime.Now;



        // Blazor LifeCycle Trigger 00
        public Tabular()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Tabular] Tabular() = BLT00");
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, $"IN Tabular() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        
        // Blazor LifeCycle Trigger 02
        protected override void OnInitialized()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Tabular] OnInitialized() - BLT02");
            try
            {                
                _TabularExtract = GetInitialTabularVM();

                //Loggy.Log(Loggy.LogType.Debug, $"IN [Tabular].OnInitialized() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 03
        protected override async Task OnInitializedAsync()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Tabular] OnInitializedAsync() - BLT03");
            try
            {
                _TabularExtract = await GetUpdatedTabularData();
                
                //Loggy.Log(Loggy.LogType.Debug, $"IN [Tabular].OnInitializedAsync() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 04 & 06
        //protected override async Task OnAfterRenderAsync(bool firstRender)
        //{
        //    //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Tabular] OnAfterRenderAsync(bool firstRender) - BLT04_BLT06");
        //    try
        //    {
        //        //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard] firstRender = {firstRender.ToString()}");                
        //        if (firstRender)
        //        {
        //            Loggy.Log(Loggy.LogType.Debug, $"IN [Tabular].OnAfterRenderAsync(bool firstRender) - FIRST RENDER - BLT04");
        //        }
        //        else
        //        {                    
        //            Loggy.Log(Loggy.LogType.Debug, $"IN [Tabular].OnAfterRenderAsync(bool firstRender) - NEXT RENDER - BLT06");
        //        }
        //        //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnInitializedAsync() - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}


        // Blazor LifeCycle Trigger 05

        protected override void OnParametersSet()
        {
            Loggy.Log(Loggy.LogType.Debug, $"START CODE for [Dashboard] OnParametersSet() - BLT05"); 
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, $"IN [Dashboard].OnParametersSet() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }


        // -----------------------------------------------------------
        // Frontend Events

        // -- TODO

        // -----------------------------------------------------------
        // Dashboard Methods

        private TabularVM GetInitialTabularVM()
        {
            Loggy.Log(Loggy.LogType.Debug, "[Tabular] Entered GetInitialTabularVM()");
            try
            {
                TabularVM tabularVM = new TabularVM();
                tabularVM.slotInfosByTime = GetInitialSlotInfoCourtAny();
                tabularVM.ExtractAsAt = DateTime.Now;
                tabularVM.slotInfosByRelayGroup = new List<SlotInfosByRelayGroup>();
                SlotInfosByRelayGroup slotInfosByRelayGroup = new SlotInfosByRelayGroup();
                slotInfosByRelayGroup.RelayGroupId = tabularVM.slotInfosByTime.First().SlotCourt;
                slotInfosByRelayGroup.slotInfos = tabularVM.slotInfosByTime;
                tabularVM.slotInfosByRelayGroup.Add(slotInfosByRelayGroup);
                
                return tabularVM;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        private List<SlotInfo> GetInitialSlotInfoCourtAny()
        {
            Loggy.Log(Loggy.LogType.Debug, "[Tabular] Entered GetInitialSlotInfoCourtAny()");
            try
            {
                List<SlotInfo> slotInfoAnyCourt = new List<SlotInfo>();

                SlotInfo slotInfo = new SlotInfo();
                slotInfo.SlotId = "1";
                slotInfo.SlotCourt = "1";
                slotInfo.SlotCreator = "SampleCreator";
                slotInfo.SlotTimeFrom = new DateTime(2000, 1, 1, 0, 0, 0);
                slotInfo.SlotTimeTo = new DateTime(2000, 1, 1, 23, 59, 59);
                slotInfo.SlotState = "Set Off";
                slotInfo.LightTimeUpdated = false;
                slotInfo.LightTimeFrom = slotInfo.SlotTimeFrom ?? new DateTime(2000, 1, 1, 0, 0, 1);
                slotInfo.LightTimeTo = slotInfo.SlotTimeFrom ?? new DateTime(2000, 1, 1, 23, 59, 59);
                slotInfo.LightState = "Lights Off";
                slotInfo.SlotType = "Bookings";

                slotInfoAnyCourt.Add(slotInfo);

                return slotInfoAnyCourt;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        private async Task<TabularVM> GetUpdatedTabularData()
        {
            Loggy.Log(Loggy.LogType.Debug, "[Tabular] Entered GetUpdatedTabularData() ");
            try
            {

                TabularVM curTabularVM = new TabularVM();

                var tokenValue = await GetAccessTokenValue();

                var latestTimeslots = new List<TimeSlotDataModel>();
                latestTimeslots = await _AppManager.HttpGetTimeslots(tokenValue);

                var curDT = DateTime.Now;
                var extractAsAt = new DateTime(curDT.Year, curDT.Month, curDT.Day, 0, 0, 0);

                var lessTimeslots = (from x in latestTimeslots
                                     where x.OriginalTimeFrom >= extractAsAt
                                     orderby x.OriginalTimeFrom, x.RelayGroupId
                                     select x).ToList();

                curTabularVM.slotInfosByTime = new List<SlotInfo>();
                curTabularVM.ExtractAsAt = extractAsAt;
                curTabularVM.slotInfosByRelayGroup = new List<SlotInfosByRelayGroup>();
                var slotInfosByRelayGroup01 = new SlotInfosByRelayGroup();
                slotInfosByRelayGroup01.slotInfos = new List<SlotInfo>();
                var slotInfosByRelayGroup02 = new SlotInfosByRelayGroup();
                slotInfosByRelayGroup02.slotInfos = new List<SlotInfo>();
                var slotInfosByRelayGroup03 = new SlotInfosByRelayGroup();
                slotInfosByRelayGroup03.slotInfos = new List<SlotInfo>();
                var slotInfosByRelayGroup04 = new SlotInfosByRelayGroup();
                slotInfosByRelayGroup04.slotInfos = new List<SlotInfo>();

                foreach (var tS in lessTimeslots)
                {

                    SlotInfo slotInfo = new SlotInfo();
                    slotInfo.SlotId = tS.Id;
                    slotInfo.SlotCourt = tS.RelayGroupId;
                    slotInfo.SlotCreator = tS.CreatedByUsername;
                    slotInfo.SlotTimeFrom = tS.OriginalTimeFrom ?? new DateTime(2000, 1, 1, 0, 0, 0);
                    slotInfo.SlotTimeTo = tS.OriginalTimeTo ?? new DateTime(2000, 1, 1, 23, 59, 59);
                    slotInfo.SlotState = (tS.OriginalState == TimeSlotDataModel.TimeSlotState.On) ? "Set On" : "Set Off";
                    slotInfo.LightTimeUpdated = tS.SunlightAwareAffected;
                    slotInfo.LightTimeFrom = tS.TimeFrom ?? tS.OriginalTimeFrom;
                    slotInfo.LightTimeTo = tS.TimeTo ?? tS.OriginalTimeTo;
                    slotInfo.LightState = (tS.State == TimeSlotDataModel.TimeSlotState.On) ? "Lights On" : "Lights Off";                    
                    if (tS.Type.ToLower() == TimeSlotMode.tstExternal.ToLower())
                    {
                        slotInfo.SlotType = "Bookings";
                    }
                    if (tS.Type.ToLower() == TimeSlotMode.tstInternal.ToLower())
                    {
                        slotInfo.SlotType = "Committee";
                    }

                    curTabularVM.slotInfosByTime.Add(slotInfo);


                    if(tS.RelayGroupId == RelayGroupKeys.rg1)
                    {
                        slotInfosByRelayGroup01.RelayGroupId = tS.RelayGroupId;
                        slotInfosByRelayGroup01.slotInfos.Add(slotInfo);
                    }
                    if (tS.RelayGroupId == RelayGroupKeys.rg2)
                    {
                        slotInfosByRelayGroup02.RelayGroupId = tS.RelayGroupId;
                        slotInfosByRelayGroup02.slotInfos.Add(slotInfo);
                    }
                    if (tS.RelayGroupId == RelayGroupKeys.rg3)
                    {
                        slotInfosByRelayGroup03.RelayGroupId = tS.RelayGroupId;
                        slotInfosByRelayGroup03.slotInfos.Add(slotInfo);
                    }
                    if (tS.RelayGroupId == RelayGroupKeys.rg4)
                    {
                        slotInfosByRelayGroup04.RelayGroupId = tS.RelayGroupId;
                        slotInfosByRelayGroup04.slotInfos.Add(slotInfo);
                    }


                }

                curTabularVM.slotInfosByRelayGroup.Add(slotInfosByRelayGroup01);
                curTabularVM.slotInfosByRelayGroup.Add(slotInfosByRelayGroup02);
                curTabularVM.slotInfosByRelayGroup.Add(slotInfosByRelayGroup03);
                curTabularVM.slotInfosByRelayGroup.Add(slotInfosByRelayGroup04);

                return curTabularVM;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }



        // HELPER METHODS
        public async Task<string> GetAccessTokenValue()
        {
            //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token String");
            try
            {
                var token = await GetAccessToken();
                string tokenValue = token.Value;
                _AppManager.SetAppAccessTokenValue(tokenValue);
                return tokenValue;
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }
        public async Task<AccessToken> GetAccessToken()
        {
            //Loggy.Log(Loggy.LogType.Debug, "Entered Get Access Token");
            try
            {
                //Loggy.Log(Loggy.LogType.Debug, "[TestPage] Call Get TokenProvider Request Access Token");
                var accessTokenResult = await _TokenProvider.RequestAccessToken();
                //Loggy.Log(Loggy.LogType.Debug, "accessTokenResult = " + accessTokenResult.ToString());
                if (accessTokenResult.TryGetToken(out var token))
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Success : Token = " + token.ToString());
                    return token;
                }
                else
                {
                  //Loggy.Log(Loggy.LogType.Debug, "in TryGetToken Fail : Retuning Null ");
                    _NavigationManager.NavigateTo("/authentication/login");
                    return null;
                }
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

    }


}
