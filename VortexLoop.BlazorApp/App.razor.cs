﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Authentication;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using VortexLoop.BlazorApp.Data;
using VortexLoop.BlazorApp.Models;

namespace VortexLoop.BlazorApp
{
    public partial class App
    {
        [Inject] NavigationManager NavigationManager { get; set; }
        [Inject] public HttpClient HttpClient { get; set; }
        [Inject] public IAccessTokenProvider TokenProvider { get; set; }


        // Blazor LifeCycle Trigger 00
        public App()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [App] App() = BLT00");
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"[App] HttpClient = {HttpClient}");
              //Loggy.Log(Loggy.LogType.Debug, $"[App] TokenProvider = {TokenProvider}");
              //Loggy.Log(Loggy.LogType.Debug, $"[App] NavigationManager = {NavigationManager}");

              //Loggy.Log(Loggy.LogType.Debug, $"IN App() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 01
        //public override async Task SetParametersAsync(ParameterView parameters)
        //{
        //    Loggy.Log(Loggy.LogType.Debug, $"START CODE for [App] SetParametersAsync(ParameterView parameters) - BLT01");
        //    try
        //    {
        //        Loggy.Log(Loggy.LogType.Debug, $"IN [App].SetParametersAsync(ParameterView parameters) - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}

        // Blazor LifeCycle Trigger 02
        protected override void OnInitialized()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [App] OnInitialized() - BLT02");
            try
            {
              //Loggy.Log(Loggy.LogType.Debug, $"[App].OnInitialized() - HttpClient = {HttpClient}");
              //Loggy.Log(Loggy.LogType.Debug, $"[App].OnInitialized() - TokenProvider = {TokenProvider}");
              //Loggy.Log(Loggy.LogType.Debug, $"[App].OnInitialized() - NavigationManager = {NavigationManager}");

              //Loggy.Log(Loggy.LogType.Debug, $"IN [App].OnInitialized() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

        // Blazor LifeCycle Trigger 03
        //protected override async Task OnInitializedAsync()
        //{
        //  //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [App] OnInitializedAsync() - BLT03");
        //    try
        //    {

        //      //Loggy.Log(Loggy.LogType.Debug, $"IN [App].OnInitializedAsync() - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}

        // Blazor LifeCycle Trigger 04 & 06
        //protected override async Task OnAfterRenderAsync(bool firstRender)
        //{
        //  //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [App] OnAfterRenderAsync(bool firstRender) - BLT04_BLT06");
        //    try
        //    {
        //        if (firstRender)
        //        {
        //          //Loggy.Log(Loggy.LogType.Debug, $"IN [App].OnAfterRenderAsync(bool firstRender) - FIRST RENDER - BLT04");
        //        }
        //        else
        //        {
        //          //Loggy.Log(Loggy.LogType.Debug, $"IN [App].OnAfterRenderAsync(bool firstRender) - NEXT RENDER - BLT06");
        //        }

        //      //Loggy.Log(Loggy.LogType.Debug, $"IN [App].OnInitializedAsync() - END OF TRY CATCH");
        //    }
        //    catch (Exception ex)
        //    {
        //        Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
        //        throw;
        //    }
        //}


        // Blazor LifeCycle Trigger 05
        protected override void OnParametersSet()
        {
          //Loggy.Log(Loggy.LogType.Debug, $"START CODE for [App] OnParametersSet() - BLT05");
            try
            {

              //Loggy.Log(Loggy.LogType.Debug, $"IN [App].OnParametersSet() - END OF TRY CATCH");
            }
            catch (Exception ex)
            {
                Loggy.Log(Loggy.LogType.Error, $"Exception = {ex.Message} | Stack Trace = {ex.StackTrace}");
                throw;
            }
        }

    }

}
